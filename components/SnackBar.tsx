import { HiOutlineX } from "react-icons/hi"

export default function SnackBar({ type, text, onClick = () => { }, show }: { type: string, text: string, onClick?, show: boolean }) {
    return <div className={(type === "success" ? "bg-green-600 " : "") + (type === "error" ? "bg-red-600 " : "") + "text-white fixed left-8 bottom-6 w-60 min-w-max px-4 py-3 gap-x-4 rounded-md shadow-xl flex flex-row justify-between items-center transition-opacity ease-in-out" + (show ? " opacity-100" : " opacity-0")}>
        <div className={!show ? "invisible" : ""}>{text}</div>
        <HiOutlineX className={show ? "hover:cursor-pointer" : "invisible"} onClick={onClick} />
    </div>
}