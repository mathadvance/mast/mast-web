const MDXComponents = {
  a: (props) => (
    <a
      {...props}
      className="blue-link"
    />
  ),
  strong: (props) => (
    <em {...props} />
  ),
  ul: (props) => (
    <ul className='list-outside list-disc pl-4 space-y-1' {...props} />
  ),
  ol: (props) => (
    <ul className='list-outside list-decimal pl-4 space-y-1' {...props} />
  ),
  li: (props) => (
    <li className='pl-1 space-y-2' {...props} />
  ),
};

export default MDXComponents;
