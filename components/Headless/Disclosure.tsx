import { Disclosure } from "@headlessui/react"
import { AiOutlineDown, AiOutlineUp } from 'react-icons/ai';

export default function DisclosureComponent({ text, labelColor = "text-white", backgroundColor, borderColor, children }: { text: string; labelColor?: string; backgroundColor: string; borderColor: string; children: any; }) {
    return <Disclosure>
        {({ open }) =>
            <div className={`border-2 rounded-lg ${borderColor} space-y-2`}>
                <Disclosure.Button className={`${backgroundColor} ${labelColor} px-3 py-2 w-full flex flex-row justify-between items-center`}>
                    <div>{text}</div>
                    <div>{open ? <AiOutlineUp /> : <AiOutlineDown />}</div>
                </Disclosure.Button>
                <Disclosure.Panel className="px-3 pb-2">
                    {children}
                </Disclosure.Panel>
            </div>
        }
    </Disclosure>
}