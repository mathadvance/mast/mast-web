import { Listbox, Transition } from '@headlessui/react'
import { AiOutlineDown, AiOutlineUp } from 'react-icons/ai'

export default function ListBox({
    keyVal = false,
    value,
    placeholder,
    onChange,
    options,
    width = 'w-60',
    disabled = false,
}: {
    keyVal?: boolean,
    value: string;
    placeholder: string;
    onChange?: any;
    options: any;
    width?: string;
    disabled?: boolean;
}) {
    return (
        <Listbox value={value} onChange={onChange} disabled={disabled}>
            {({ open }) => (
                <>
                    <Listbox.Button className=
                        {`flex flex-row justify-between items-center w-full px-4 h-11 rounded-md ring-1 ring-gray-200 dark:ring-gray-800 focus:outline-none focus:ring-blue-500 dark:focus:ring-blue-600 bg-white dark:bg-gray-900 ${disabled && 'cursor-not-allowed'}`}>
                        <div className={value ? '' : 'text-gray-500 dark:text-gray-400'}>
                            {value ? (keyVal ? options[value] : value) : placeholder}
                        </div>
                        <div className='text-gray-500 dark:text-gray-400' >
                            {open ? <AiOutlineUp /> : <AiOutlineDown />}
                        </div>
                    </Listbox.Button>
                    <Transition
                        enter="transition duration-100 ease-out"
                        enterFrom="transform scale-95 opacity-0"
                        enterTo="transform scale-100 opacity-100"
                        leave="transition duration-75 ease-out"
                        leaveFrom="transform scale-100 opacity-100"
                        leaveTo="transform scale-95 opacity-0"
                    >
                        <Listbox.Options className={`absolute ${width} min-w-max max-h-40 overflow-y-auto border bg-white dark:bg-gray-900 rounded-lg shadow`}>
                            {keyVal ? Object.keys(options).map((key) =>
                                <Listbox.Option
                                    key={key}
                                    value={key}
                                >
                                    {({ active, selected }) => (
                                        <div className={`px-4 py-2
                                        ${selected ? 'bg-blue-300' : (active ? 'bg-gray-200' : '')
                                            }`}>{options[key]}</div>
                                    )}
                                </Listbox.Option>
                            ) : options.map((option, index) =>
                                <Listbox.Option
                                    key={index}
                                    value={option}
                                >
                                    {({ active, selected }) => (
                                        <div className={`px-4 py-2
                                            ${selected ? 'bg-blue-300' : (active ? 'bg-gray-200' : '')
                                            }`}>{option}</div>
                                    )}
                                </Listbox.Option>
                            )}
                        </Listbox.Options>
                    </Transition>
                </>
            )
            }
        </Listbox >
    )
}