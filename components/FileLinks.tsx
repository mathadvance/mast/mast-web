const apiPath = "/api/files"

export function DiagnosticLink({ season, text = `Season ${season}` }: { season: number, text?: string }) {
    return <a href={`${apiPath}/diagnostics/${season}`} className="blue-link">{text}</a>
}

export function DiagnosticLinks({ seasons }: { seasons: number }) {
    return <ul className="list-disc list-inside">
        {Array.from(Array(Number(seasons)), (e, idx) => {
        const season: number = Number(seasons) - idx;
        return <li><DiagnosticLink season={season} /></li>})}
    </ul>
}

export function UnitLink({ code, text }: { code: string, text: string }) {
    // "Code" can contain info like CQV-Perspectives?version=1
    // "Text" is usually the fully qualified unit name, like "Factoring a Prime" in NQV-Prime
    return <a href={`${apiPath}/units/${code}`} className="blue-link">{text}</a>
}

export function ApplicationLink({ username, text = "Application PDF" }: { username: string, text?: string }) {
    return <a href={`${apiPath}/applications/${username}`} className="blue-link">{text}</a>
}
