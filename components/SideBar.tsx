import NextLink from "next/link";

import { useAuth } from "@/contexts/AuthProvider";
import { useTheme } from "@/contexts/ThemeProvider";

import {
  FaHome,
  FaCog,
  FaInfo,
  FaQuestion,
  FaDollarSign,
  FaPaperPlane,
  FaUser,
  FaFileAlt,
  FaDoorOpen,
  FaKey,
  FaPlus,
  FaPencilAlt,
} from "react-icons/fa";
import { useRouter } from "next/router";

export default function SideBar() {

  const { user } = useAuth();

  const { asPath } = useRouter();
  const paths = asPath.split("/");
  paths.shift(); // Because asPath has a leading /

  const { sideBarColor } = useTheme();

  function Wrapper({ children }: { children?}) {
    return (
      <div
        className={`grid grid-cols-1 widephone:grid-cols-2 sm:grid-cols-1 gap-y-1.5  rounded-xl border p-4 ${sideBarColor === "pink"
          ? `bg-pink-100 dark:bg-pink-600/25 border-pink-500`
          : `bg-blue-200 dark:bg-opacity-30 dark:bg-blue-600 border-blue-500`
          }`}
      >
        {children}
      </div>
    );
  }

  function SideRow({ needsUser = false, link, label, icon }) {

    if (needsUser && !user) {
      return null;
    }

    return (
      <NextLink href={link}>
        <a
          className={`flex gap-x-2 items-center ${sideBarColor === "pink"
            ? "pink-link"
            : "text-blue-700 hover:text-blue-600 dark:text-blue-400 dark:hover:text-blue-500 hover:underline"
            }`}
        >
          <div className="text-lg">{icon}</div>
          <div>{label}</div>
        </a>
      </NextLink>
    );
  }

  if (paths[0] === "profile" && [!paths[1] || paths[1] === "pat"]) {
    return (
      <Wrapper>
        <SideRow link="/profile/pat" label="Tokens" icon={<FaKey />} />
        {paths[1] && paths[1] === "pat" && (
          <SideRow
            link="/profile/pat/generate"
            label="Generate PAT"
            icon={<FaPlus />}
          />
        )}
        {user.power >= 4 && <SideRow
          link="/profile/bio"
          label="Staff Bio"
          icon={<FaPencilAlt />}
        />}
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <SideRow needsUser link="/home" label="Home" icon={<FaHome />} />
      <SideRow needsUser link="/settings" label="Settings" icon={<FaCog />} />
      <SideRow link="/about" label="About" icon={<FaInfo />} />
      <SideRow link="/staff" label="Staff" icon={<FaUser />} />
      <SideRow link="/apply" label="Apply" icon={<FaPaperPlane />} />
      <SideRow link="/payment" label="Payment" icon={<FaDollarSign />} />
      <SideRow link="/resources" label="Resources" icon={<FaFileAlt />} />
      <SideRow link="/faq" label="FAQ" icon={<FaQuestion />} />
      <SideRow
        needsUser
        link="/signout"
        label="Sign Out"
        icon={<FaDoorOpen />}
      />
    </Wrapper>
  );
}
