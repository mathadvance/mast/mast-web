import Head from "next/head";
import MDXComponents from "@/components/MDXComponents";
import { getMDXComponent } from "mdx-bundler/client";
import { useMemo } from "react";

function PageRender({ props }) {
  const Content = useMemo(() => getMDXComponent(props.code), [props.code]);
  return (
    <>
      <Head>
        <title>
          {props.data.title ? `${props.data.title} - MAST` : "MAST"}
        </title>
        <meta
          name="description"
          content={`${props.data.desc || props.data.title}`}
        />
      </Head>
      <h1>{props.data.title}</h1>
      <Content components={MDXComponents} />
    </>
  );
}

export default PageRender;
