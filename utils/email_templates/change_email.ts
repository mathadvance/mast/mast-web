export default function change_email({
  username = "",
  verification_link = "",
}: {
  username: string;
  verification_link: string;
}) {
  return `Greetings, ${username}. To verify your new email for the MAST website, please click the following link:

  ${verification_link}

The verification link will expire in 30 minutes. If it does, you may request another at your profile page by sending another email change request.
    
If you did not issue this request, you may ignore this email.`;
}

export function change_email_notify({
  username = "",
  new_email = "",
}: {
  username: string;
  new_email: string;
}) {
  // Notify old email of the change email request
  return `Greetings, ${username}. You have requested to change your email address for your MAST account to ${new_email}.

Until you verify the new email address, this email will remain associated with your MAST account.

If you did not make this request, then please contact dchen@mathadvance.org.`;
}
