export default function UnitChecked({
  first_name,
  unit,
  instructor,
  comments,
}: {
  first_name: string;
  unit: string;
  instructor: string;
  comments: string;
}) {
  return `Greetings, ${first_name}. The unit you submitted, ${unit}, has been checked by ${instructor}. Their comments:

${comments}`;
}
