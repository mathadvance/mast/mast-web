export default function rejection({
    first_name = ""
}: {
    first_name: string;
}) {
    return `Hello, ${first_name}. It is our great regret to say that you were not accepted for the MAST program this time around. We strongly encourage that you try again next time applications roll around.

All the best,
The MAST Team`;
}