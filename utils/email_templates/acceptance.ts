export default function acceptance({
    first_name = ""
}: {
    first_name: string;
}) {
    return `Hello, ${first_name}. Congratulations on getting accepted to the MAST program! You now can access content on the MAST website; simply log in to your account and visit your homepage to do so.

It is optional, but highly recommended, that you join the MAST Discord server at

    ${process.env.DISCORD}.

We hope to see you around!

The MAST Team`;
}