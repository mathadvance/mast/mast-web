export default function password_reset({ username = "", reset_link = "", }: { username?: string, reset_link?: string }) {
    return `The username of your MAST account is ${username}.

To reset your password, click the following link:
    
    ${reset_link}`;
}