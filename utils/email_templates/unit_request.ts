export default function UnitRequest({
  username,
  unit,
}: {
  username: string;
  unit: string;
}) {
  return `${username} has requested ${unit} on the MAST website.`;
}
