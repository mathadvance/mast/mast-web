export default function verify_email({ username = "", verification_link = "" }: { username: string, verification_link: string }) {
    return `
Greetings, ${username}. To verify your email for the MAST website, please click the following link: ${verification_link}

The verification link will expire in 30 minutes. If it does, you may request another at your home page:

    ${process.env.DOMAIN}/home

(You must be signed in to see the home page.)

If you did not create an account on the MAST website, you may ignore this email.`;
}