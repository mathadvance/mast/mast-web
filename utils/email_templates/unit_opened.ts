export default function UnitOpened({
    first_name,
    unit,
}: {
    first_name: string;
    unit: string;
}) {
    return `Greetings, ${first_name}. The unit you requested, ${unit}, has been opened on the MAST website.`;
}