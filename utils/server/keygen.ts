import str from "@supercharge/strings";

export default function keygen() {
  return str.random(32);
}
