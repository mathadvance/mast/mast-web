// Rate limiting function - to prevent password brute forcing
// We use the sliding window counter as described in https://blog.logrocket.com/rate-limiting-node-js/
// Each window is 1 minute (60,000 ms) and we sum up the last 10 windows
// We limit to 100 requests every 10 minutes - this should not be exceeded by any legitimate actor.
// Returns true if we should rate limit, false otherwise

import { redis_rate_limit } from "@/utils/server/redis";

export async function ip_rate_limit(ip: string) {
    let redis_time: number = Math.floor(Date.now() / 60000);
    let redis_count: number = await redis_rate_limit.get(ip + "_" + redis_time);
    if (redis_count) {
        redis_rate_limit.set(ip + "_" + redis_time, Number(redis_count) + 1, "EX", 69000); // A bit of buffer from the required 60,000 just in case
    } else {
        redis_rate_limit.set(ip + "_" + redis_time, 1, "EX", 69000);
    }
    let redis_total = 0;
    for (let i: number = 0; i < 10; i++) {
        let time: number = redis_time - i;
        let count = await redis_rate_limit.get(ip + "_" + time) || 0;
        redis_total += Number(count);
    }
    if (redis_total > 100) {
        return true
    } else {
        return false;
    }
}