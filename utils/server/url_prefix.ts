// "Only absolute URLs are supported" on server side

const url_prefix =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:3000"
    : process.env.DOMAIN;

export default url_prefix;
