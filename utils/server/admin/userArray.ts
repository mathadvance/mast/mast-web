import url_prefix from "@/utils/server/url_prefix";
import cookie from 'cookie'
import { mastDB } from "@/utils/server/mongodb";

export async function getUserArray(context) {

    if (!context.req.headers.cookie) {
        return
    }

    const cookies = cookie.parse(context.req.headers.cookie)

    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token: cookies.session,
            tokenType: 'session'
        })
    })

    if (authRes.status >= 300) {
        return;
    }

    const { user } = JSON.parse(await authRes.text())
    if (user.power < 4) {
        return;
    }

    const userArray = await mastDB.collection("users").find({}, { projection: { _id: 0, destructionDate: 0, hashedPassword: 0, PATs: 0 } }).toArray()
    userArray.sort((a, b) => (a.power > b.power) ? -1 : 1)
    return userArray;
}