import Head from "next/head";

import AuthProvider from "@/contexts/AuthProvider";
import ThemeProvider from "@/contexts/ThemeProvider";
import ToastProvider from "@/contexts/ToastProvider";

import "@/styles/global.css";
import AppPage from "@/components/AppPage";

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <ThemeProvider>
        <ToastProvider>
          <Head>
            <title>MAST</title>
          </Head>
          <AppPage>
            <Component {...pageProps} />
          </AppPage>
        </ToastProvider>
      </ThemeProvider>
    </AuthProvider>
  );
}

export default MyApp;
