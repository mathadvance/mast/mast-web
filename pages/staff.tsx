import url_prefix from "@/utils/server/url_prefix";

export default function Staff({ bios }) {

  return <>
    <h1>Staff</h1>
    {Object.keys(bios).map((key) => {
      const bio = bios[key];
      return <div className="space-y-2" key={key}>
        <h2>{bio.first_name} {bio.last_name}</h2>
        <div className="flex gap-x-4">
          <div>{bio.data.text}</div>
          {bio.data.ext && <img
            src={`${process.env.NEXT_PUBLIC_ASSETS_URL}/${key}.${bio.data.ext}`}
            width={150}
            height={150}
          />}
        </div>
      </div>
    })}</>
}

export async function getStaticProps() {
  const res = await fetch(`${url_prefix}/api/public/bios`, { method: "GET" })

  if (res.status >= 300) {
    return {
      props: {
        bios: {}
      },
      revalidate: 120
    }
  }

  const bios = await res.json()
  return {
    props: {
      bios
    },
    revalidate: 120
  }
}