import fs from "fs"
import path from "path"
import yaml from "yaml"

export default async (req, res) => {
    const units = {};
    const unitsPath = path.join(process.env.RESOURCE_DIR, "content", "units")
    const unitDirs = await fs.promises.readdir(unitsPath)
    for (const unit of unitDirs) {
        const data = await fs.promises.readFile(path.join(unitsPath, unit, "metadata.yml"), 'utf8')
        const unitObject = yaml.parse(data)

        const files = await fs.promises.readdir(path.join(unitsPath, unit));
        unitObject.versions = files.filter(file => {
            return /problems-\d+.tex/.test(file)
        }).map((file) => {
            return file.split("-")[1].split(".")[0];
        })
        unitObject.code = unit.split("-")[0];
        const short = unit.split("-")[1];
        units[short] = unitObject
    }
    return res.status(200).json(units);
}