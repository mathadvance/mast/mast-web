import { mastDB } from "@/utils/server/mongodb"
import fs from "fs";
import path from "path";

export default async (req, res) => {
    const staff = await mastDB.collection("users").find({ power: { $gte: 4 } }).toArray()
    const bios = {};
    for (const user in staff) {
        if (staff[user].data.bio) {
            bios[staff[user].username] = {
                first_name: staff[user].first_name,
                last_name: staff[user].last_name,
                data: staff[user].data.bio
            };
        }
    }
    return res.status(200).json(bios)
}