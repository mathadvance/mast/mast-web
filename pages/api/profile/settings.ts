import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";

export default async (req, res) => {
  const request = JSON.parse(req.body);

  let token;

  if (request.tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (request.tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid token type sent.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token: request.token || token, // empty request.token is supposed to be sent if tokenType is browser
      tokenType: request.tokenType,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Invalid token.");
  }

  const username = JSON.parse(await authRes.text()).user.username;
  mastDB.collection("users").updateOne(
    {
      username,
    },
    {
      $set: {
        Settings: request.Settings,
      },
    }
  );
  res.status(200).send("Successfully updated user settings.");
};
