import { createNoReplyMail } from "@/utils/server/email";
import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies"
import acceptance from "@/utils/email_templates/acceptance";
import rejection from "@/utils/email_templates/rejection";

export default async (req, res) => {
    const { tokenType, username, decision } = req.body;
    let { token } = req.body;

    if (tokenType === "session") {
        const cookies = new Cookies(req, res, { secure: true });
        token = cookies.get("session");
    } else if (tokenType != "PAT") {
        return res.status(400).send("Invalid token type sent.");
    }

    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token,
            tokenType,
        }),
    });

    if (authRes.status >= 300) {
        return res.status(401).send("Invalid token.");
    }

    const { user } = JSON.parse(await authRes.text())

    if (user.power < 4) {
        return res.status(403).send('User does not have the permission to manage applications.')
    }

    const applicant = await mastDB.collection('users').findOne({ username });
    if (!applicant) {
        return res.status(404).send('Applicant not found. Input the username of a valid applicant.')
    }
    if (applicant.power != 2) {
        return res.status(404).send('Application status of a non-applicant cannot be set.')
    }

    if (decision != 'accept' && decision != 'reject') {
        return res.status(400).send('Decision must either be "accept" or "reject".')
    }

    await mastDB.collection('applications').updateOne({ username }, { $set: { decision } })
    if (decision === "accept") {
        const text = acceptance({ first_name: applicant.first_name })
        createNoReplyMail({
            recipient: applicant.email,
            subject: 'MAST Acceptance',
            text,
        })
        await mastDB.collection('users').updateOne({ username }, { $set:
            { power: 3, 'data.units': {} }
        })
    } else {
        const text = rejection({ first_name: applicant.first_name })
        createNoReplyMail({
            recipient: applicant.email,
            subject: 'MAST Decision',
            text,
        })
    }

    return res.status(200).send('Application decision set.')
}