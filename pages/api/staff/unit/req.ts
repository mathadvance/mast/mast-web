import { mastDB } from "@/utils/server/mongodb"
import url_prefix from "@/utils/server/url_prefix";
import Cookies from 'cookies';
import unit_opened from "@/utils/email_templates/unit_opened";
import { createNoReplyMail } from "@/utils/server/email";

export default async (req, res) => {

    const { tokenType, username, unit } = JSON.parse(req.body);
    let { token } = JSON.parse(req.body);

    if (tokenType === "session") {
        const cookies = new Cookies(req, res, { secure: true });
        token = cookies.get("session");
    } else if (tokenType != "PAT") {
        return res.status(400).send("Invalid token type sent.");
    }

    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token,
            tokenType,
        }),
    });

    if (authRes.status >= 300) {
        return res.status(401).send("Invalid token.");
    }

    const { user } = JSON.parse(await authRes.text())

    if (user.power < 4) {
        return res.status(403).send('User does not have the permission to manage unit request.')
    }

    const student = await mastDB.collection('users').findOne({ username });
    if (!student) {
        return res.status(404).send('Student not found. Input the username of a valid student.')
    }
    if (student.power != 3) {
        return res.status(403).send('Unit status of a non-student cannot be set.')
    }

    const obj = {};
    const key = `data.units.${unit}.status`
    obj[key] = 'open'

    await mastDB.collection('users').updateOne({ username }, { $set: obj })

    const text = unit_opened({ first_name: student.first_name, unit })
    createNoReplyMail({
        recipient: student.email,
        subject: `Unit ${unit} Opened`,
        text,
    })

    return res.status(200).send('Unit opened.')
}