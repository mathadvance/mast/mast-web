import url_prefix from "@/utils/server/url_prefix";
import { mastDB } from "@/utils/server/mongodb";
import Cookies from "cookies"

// Returns an array of all users

export default async (req, res) => {
	const { tokenType } = req.body;
	let { token } = req.body;

	if (tokenType === "session") {
		const cookies = new Cookies(req, res, { secure: true });
		token = cookies.get("session");
	} else if (tokenType != "PAT") {
		return res.status(400).send("Invalid token type sent.");
	}

	const authRes = await fetch(`${url_prefix}/api/auth/token`, {
		method: "POST",
		body: JSON.stringify({
			token,
			tokenType,
		}),
	});

	if (authRes.status >= 300) {
		return res.status(401).send("Invalid token.");
	}

	const { user } = JSON.parse(await authRes.text())

	if (user.power < 4) {
		return res.status(403).send('User does not have the permission to fetch the list of users.')
	}

	const users = await mastDB.collection("users").find({}, { projection: { _id: 0, destructionDate: 0, hashedPassword: 0, PATs: 0 } }).toArray()
	return res.status(200).send(JSON.stringify(users));
}
