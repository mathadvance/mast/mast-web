import { mastDB } from "@/utils/server/mongodb";
import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {
  // User A changes User B's power
  // token, tokenType are for User A (their power must be 5 or higher)
  // targetUsername, power are for User B (only can go up to 5)
  const request = JSON.parse(req.body);

  let token;

  if (request.tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (request.tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid token type sent.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token: request.token || token, // empty request.token is supposed to be sent if tokenType is browser
      tokenType: request.tokenType,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Invalid token.");
  }

  const user = JSON.parse(await authRes.text()).user;

  if (user.power < 5) {
    return res.status(403).send("Acting user's power is not high enough.");
  }

  if (isNaN(request.power) || request.power > 5 || request.power < 0) {
    return res.status(400).send("Cannot set power outside the range 0-5.");
  }

  if (parseInt(request.power) === NaN) {
    return res.status(400).send("Power must be an integer.")
  }

  const targetUser = await mastDB.collection("users").findOne({ username: request.targetUsername });

  if (!targetUser) {
    return res.status(400).send("Target user with requested username does not exist.");
  }

  if (targetUser.power === 6) {
    return res.status(403).send("Cannot change the power of a superadmin.")
  }

  if (targetUser.power === 5 && user.power === 5) {
    return res.status(403).send("Cannot change the power of an admin unless you are a superadmin.")
  }

  await mastDB
    .collection("users")
    .updateOne(
      { username: request.targetUsername },
      { $set: { power: parseInt(request.power) } }
    )
  return res.status(200).send("User power changed.");
};
