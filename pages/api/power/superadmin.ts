import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {
  // Takes username/password of superadmin, demotes superadmin to regular admin, and makes target user a superadmin
  const request = JSON.parse(req.body);

  const authRes = await fetch(`${url_prefix}/api/auth/check-credentials`, {
    method: "POST",
    body: JSON.stringify({
      username: request.username,
      password: request.password,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Incorrect username/password combination.");
  }

  const user = await mastDB
    .collection("users")
    .findOne({ username: request.username });

  if (user.power != 6) {
    return res.status(403).send("You are not a superadmin.");
  }

  const newSuperadmin = await mastDB.collection("users").findOne({ username: request.targetUsername })

  if (!newSuperadmin) {
    res
      .status(404)
      .send("New superadmin with requested username does not exist.");
    return;
  }

  mastDB
    .collection("users")
    .updateOne({ username: request.targetUsername }, { $set: { power: 6 } });

  mastDB
    .collection("users")
    .updateOne({ username: request.username }, { $set: { power: 5 } });

  return res.status(200).send("New superadmin set.");
};
