import { mastDB } from "@/utils/server/mongodb";
import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";

// Become an applicant (1 to 2)

export default async (req, res) => {
  const request = JSON.parse(req.body);

  let token;

  if (request.tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (request.tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid token type sent.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token: request.token || token, // empty request.token is supposed to be sent if tokenType is browser
      tokenType: request.tokenType,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Invalid token.");
  }

  const user = JSON.parse(await authRes.text()).user;
  if (user.power === 1) {
    mastDB
      .collection("users")
      .updateOne({ username: user.username }, { $set: { power: 2 } });
    return res.status(200).send("Successfully changed power.");
  } else {
    return res
      .status(403)
      .send("Cannot set power to 2 unless user power is 1.");
  }
};
