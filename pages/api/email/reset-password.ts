import { mastDB } from "@/utils/server/mongodb";
import { redis_passwordResetIDS } from "@/utils/server/redis";
import argon2 from "argon2";

export default async (req, res) => {
  const request = JSON.parse(req.body);
  if (request.action != "verify" && request.action != "change") {
    return res
      .status(404)
      .send(`The requested action is neither "verify" nor "change".`);
  }
  const redisValString = await redis_passwordResetIDS.get(request.key);
  if (!redisValString) {
    res.status(401).send("The password reset key is invalid.");
    return;
  }
  const redisValObject = JSON.parse(redisValString);
  const user = await mastDB
    .collection("users")
    .findOne({ username: redisValObject.username });
  if (
    redisValObject.timestamp !=
    user.Timestamps.most_recent_password_reset_timestamp
  ) {
    res
      .status(401)
      .send("This is not the most recently issued password reset key.");
    return;
  }
  if (request.action === "verify") {
    res.status(200).send("This password reset key is valid.");
    return;
  }
  if (request.action === "change") {
    const hashedPassword = await argon2.hash(request.password);
    redis_passwordResetIDS.del(request.key);
    mastDB
      .collection("users")
      .updateOne(
        { username: redisValObject.username },
        { $set: { hashedPassword: hashedPassword } }
      );
    res.status(200).send("Your password has been reset.");
    return;
  }
};
