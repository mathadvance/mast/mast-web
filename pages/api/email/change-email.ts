import { createNoReplyMail } from "@/utils/server/email";
import change_email, {
  change_email_notify,
} from "@/utils/email_templates/change_email";
import keygen from "@/utils/server/keygen";
import { redis_changeEmailIDS } from "@/utils/server/redis";
import { mastDB } from "@/utils/server/mongodb";
import EmailValidator from "email-validator";

import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {
  const request = JSON.parse(req.body);
  if (request.action === "send") {
    if (!EmailValidator.validate(request.newEmail)) {
      res.status(400).send("You must input a valid email.");
      return;
    }

    const email_proxy = await mastDB.collection("email_proxy").findOne({ email: request.newEmail })
    if (email_proxy) {
      res.status(403).send("This email address is already taken.")
    }

    let token;
    const tokenType = request.tokenType;
    if (tokenType === "session") {
      const cookies = new Cookies(req, res, { secure: true });
      token = cookies.get("session");
    } else if (tokenType === "PAT") {
      token = request.token;
    } else {
      return res.status(400).send("Token type missing or invalid.");
    }
    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
      method: "POST",
      body: JSON.stringify({
        token,
        tokenType,
      }),
    });
    if (authRes.status < 300) {
      const username = JSON.parse(await authRes.text()).user.username;

      const notifyText = change_email_notify({
        username,
        new_email: request.newEmail,
      });

      createNoReplyMail({
        recipient: request.oldEmail,
        subject: "Email change requested for MAST",
        text: notifyText,
      });

      const verificationId = keygen();
      const verification_link = `${process.env.NODE_ENV === "development"
        ? `http://localhost:3000`
        : process.env.DOMAIN
        }/profile/change-email?key=${verificationId}`;

      const text = change_email({
        username,
        verification_link,
      });
      request;

      createNoReplyMail({
        recipient: request.newEmail,
        subject: "Confirm your new email for MAST",
        text,
      });

      const timestamp = Date.now();

      redis_changeEmailIDS.set(
        verificationId,
        JSON.stringify({
          username,
          timestamp,
          email: request.newEmail,
        }),
        "EX",
        60 * 30 // Key expires 30 minutes after setting
      );

      mastDB.collection("users").updateOne(
        { username },
        {
          $set: {
            "Timestamps.most_recent_email_change_timestamp": timestamp,
          },
        }
      );

      mastDB.collection("email_proxy").updateOne(
        {
          username,
        },
        {
          $set: { email: request.newEmail },
        }
      );

      return res.status(200).send("Email verification sent.");
    } else {
      return res.status(401).send("Invalid token sent.");
    }
  }
  if (request.action === "verify") {
    const redisValString = await redis_changeEmailIDS.get(request.key);
    const redisValObject = JSON.parse(redisValString);
    const user = await mastDB
      .collection("users")
      .findOne({ username: redisValObject.username });
    if (
      redisValObject.timestamp !=
      user.Timestamps.most_recent_email_change_timestamp
    ) {
      res
        .status(401)
        .send("This is not the most recently issued verification key.");
      return;
    }
    mastDB
      .collection("users")
      .updateOne(
        { username: redisValObject.username },
        { $set: { email: redisValObject.email } }
      );
    mastDB.collection("email_proxy").updateOne({ username: redisValObject.username }, { $set: { email: redisValObject.email } })
    redis_changeEmailIDS.del(request.key);
    res.status(200).json({ email: redisValObject.email });
    return;
  }
  res
    .status(404)
    .send(`The requested action is neither "verify" nor "change".`);
  return;
};
