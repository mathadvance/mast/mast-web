import { mastDB } from "@/utils/server/mongodb";
import { redis_emailVerificationIDs } from "@/utils/server/redis";
import { createNoReplyMail } from "@/utils/server/email";
import keygen from "@/utils/server/keygen";
import verify_email from "@/utils/email_templates/verify_email";
import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {
  const request = JSON.parse(req.body);

  let token;
  const tokenType = request.tokenType;

  if (tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Empty or invalid token type.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      tokenType,
      token,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Invalid token.");
  }

  const user = JSON.parse(await authRes.text()).user;

  const timestamp = Date.now();

  mastDB.collection("users").updateOne(
    { username: user.username },
    {
      $set: {
        "Timestamps.most_recent_email_verification_timestamp": timestamp,
      },
    }
  );

  const emailVerificationID = keygen();

  const redisValString = JSON.stringify({
    username: user.username,
    timestamp,
  });

  redis_emailVerificationIDs.set(
    emailVerificationID,
    redisValString,
    "EX",
    60 * 30
  ); // 30 minutes to verify

  const verification_link = `${process.env.NODE_ENV === "development"
    ? `http://localhost:3000`
    : process.env.DOMAIN
    }/profile/verify-email?key=${emailVerificationID}`; // Change what URL is being sent in dev because test emails come from test domain, not actual production domain, so why link to production domain?

  const text = verify_email({ username: user.username, verification_link });

  await createNoReplyMail({
    recipient: user.email,
    subject: "Verify your email for MAST",
    text,
  });

  return res.status(200).send("Successfully sent verification email.");
};
