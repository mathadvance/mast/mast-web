import formidable from "formidable";
import mv from "mv";
import path from "path";
import Cookies from "cookies";

import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";

const maxFileSize = 512 * 1024; // 512 KB, very generous limit for LaTeX

export default async (req, res) => {
  await new Promise<void>((resolve) => {
    const form = formidable({ maxFileSize });
    form.onPart = function (part) {
      if (!part.filename || !part.mime) {
        form.handlePart(part);
      } else {
        if (part.mime === "application/pdf") {
          // Can get spoofed by renaming file on client desktop, but doesn't pose any real issue besides annoyance
          form.handlePart(part);
        }
      }
    };
    form.parse(req, (err, fields, files) => {
      (async () => {
        let token;
        if (fields.tokenType === "session") {
          const cookies = new Cookies(req, res, { secure: true });
          token = cookies.get("session");
        } else if (fields.tokenType === "PAT") {
          token = fields.token;
        } else {
          return res.status(400).send(`Invalid 'tokenType' sent.`);
        }
        const authRes = await fetch(`${url_prefix}/api/auth/token`, {
          method: "POST",
          body: JSON.stringify({
            token,
            tokenType: fields.tokenType,
          }),
        });
        if (authRes.status < 300) {
          const authJSON = await authRes.text();
          const { user } = JSON.parse(authJSON);

          if (user.data.applied) {
            return res.status(403).send("You've already applied!");
          }
          if (err) {
            console.log(err);
            if (err.toString().indexOf("maxFileSize exceeded") > -1) {
              return res
                .status(400)
                .send("Maximum file size of 512 KB exceeded.");
            } else {
              return res.status(500).send(err);
            }
          }
          if (!files.PDF) {
            return res.status(400).send("The file sent is not a valid PDF.");
          }

          // at this point we know everything succeeds, write stuff to disk

          mastDB.collection("users").updateOne(
            { username: user.username },
            {
              $set: {
                "data.applied": true,
              },
            }
          );

          mastDB.collection("applications").insertOne({
            username: user.username,
            first_name: user.first_name,
            last_name: user.last_name,
            answers: JSON.parse(fields.answers),
          });

          mv(
            files.PDF.path,
            path.join(
              process.env.RESOURCE_DIR,
              "uploads",
              "applications",
              `${user.username}.pdf`
            ),
            { mkdirp: true },
            (err) => {
              if (err) console.log(err);
            }
          );

          return res.status(201).send("Application successfully submitted.");
        } else {
          return res.status(400).send("Invalid token sent.");
        }
      })();
    });
  });
};

export const config = {
  api: {
    bodyParser: false,
  },
};
