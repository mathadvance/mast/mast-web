import UnitRequest from "@/utils/email_templates/unit_request";
import { createNoReplyMail } from "@/utils/server/email";
import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";

export default async (req, res) => {
  const { tokenType, unit, action } = JSON.parse(req.body);
  let { token } = JSON.parse(req.body);

  if (tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (tokenType != "PAT") {
    return res.status(400).send("Invalid token type sent.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(401).send("Invalid token.");
  }

  const { user } = JSON.parse(await authRes.text());

  if (user.power != 3) {
    return res.status(403).send("User is not a student.");
  }

  function setStatus(status) {
    const obj = {};
    obj[`data.units.${unit}.status`] = status;
    mastDB.collection("users").updateOne(
      { username: user.username },
      {
        $set: obj,
      }
    );
  }

  if (action === "interest") {
    if (
      user.data.units &&
      user.data.units[unit] &&
      user.data.units[unit].status != "dropped"
    ) {
      return res
        .status(403)
        .send(
          `Cannot mark interest in a unit with status ${user.data.units[unit].status}.`
        );
    }
    setStatus("interested");
    return res.status(200).send("Unit interest marked.");
  } else if (action === "request") {
    if (
      user.data.units &&
      user.data.units[unit] &&
      !(
        user.data.units[unit].status != "dropped" ||
        user.data.units[unit].status != "interested"
      )
    ) {
      return res
        .status(403)
        .send(
          `Cannot request a unit with status ${user.data.units[unit].status}.`
        );
    }
    setStatus("requested");
    const staff = await mastDB
      .collection("users")
      .find(
        { power: { $gte: 4 } },
        {
          projection: {
            _id: 0,
            destructionDate: 0,
            hashedPassword: 0,
            PATs: 0,
          },
        }
      )
      .toArray();
    const staff_emails = staff.map((user) => user.email);
    createNoReplyMail({
      recipient: staff_emails,
      subject: `${user.username} has requested ${unit}`,
      text: UnitRequest({ username: user.username, unit }),
    });
    console.log(staff_emails);
    return res.status(200).send("Unit requested.");
  } else if (action === "drop") {
    if (
      user.data.units &&
      user.data.units[unit] &&
      !(
        user.data.units[unit].status === "interested" ||
        user.data.units[unit].status === "requested"
      )
    ) {
      return res
        .status(403)
        .send(
          `Cannot drop a unit with status ${user.data.units[unit].status}.`
        );
    }
    setStatus("dropped");
    return res.status(200).send("Unit dropped.");
  } else {
    return res.status(400).send("Invalid action sent.");
  }
};
