import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";

export default async (req, res) => {
    const request = JSON.parse(req.body);
    // tokenType: string ("session", "PAT")
    // token: string if tokenType = "PAT", and null if tokenType = "session" (read from cookies)
    // text: Text of bio to be set.
    let token;
    const tokenType = request.tokenType;
    if (tokenType === "session") {
        const cookies = new Cookies(req, res, { secure: true })
        token = cookies.get("session")
    } else if (tokenType === "PAT") {
        token = request.token
    } else {
        return res.status(400).send("Token type missing or invalid.");
    }
    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token,
            tokenType,
        }),
    });
    if (authRes.status >= 300) {
        return res.status(401).send("Authentication failed.")
    }
    const user = JSON.parse(await authRes.text()).user
    if (user.power < 4) {
        return res.status(403).send("You do not have permissions to set a staff bio.")
    }
    mastDB.collection("users").updateOne({
        username: user.username,
    }, {
        $set: {
            "data.bio.text": request.text
        }
    })
    return res.status(200).send("Bio successfully updated.")
}