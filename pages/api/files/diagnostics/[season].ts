import path from "path";
import fs from "fs";

export default async (req, res) => {

    const { season } = req.query

    const filePath = path.join(process.env.RESOURCE_DIR,
        "content",
        "diagnostics",
        `season${season}`,
        `season${season}.pdf`)

    try {

        const pdfBuffer = fs.readFileSync(filePath)

        res.setHeader('Content-Disposition', `attachment; filename=season${season}.pdf`)
        res.setHeader('Content-Type', 'application/pdf')
        res.send(pdfBuffer);
    } catch (err) {
        console.log(err)
        res.status(404).send("File could not be found.")
    }
}
