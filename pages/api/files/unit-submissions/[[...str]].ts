import path from "path";
import fs from "fs";
import Cookies from "cookies"
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {

    let [username, unit] = req.query.str
    let token = req.query.token
    let tokenType

    if (!token) {
        const cookies = new Cookies(req, res, { secure: true })
        token = cookies.get("session")
        tokenType = "session"
    } else {
        tokenType = "PAT"
    }

    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token,
            tokenType
        })
    })

    if (authRes.status >= 300) {
        return res.status(401).send("Failed to authenticate.")
    }

    const user = JSON.parse(await authRes.text()).user

    if (user.power < 4) {
        return res.status(403).send("Your power is not high enough to access unit submissions.")
    }

    const filePath = path.join(process.env.RESOURCE_DIR,
        "uploads",
        "units",
        username,
        `${unit}.pdf`
    )

    try {

        const pdfBuffer = fs.readFileSync(filePath)

        res.setHeader('Content-Disposition', `attachment; filename=${username}_${unit}.pdf`)
        res.setHeader('Content-Type', 'application/pdf')
        res.send(pdfBuffer);
    } catch (err) {
        console.log(err)
        res.status(404).send("File could not be found.")
    }
}
