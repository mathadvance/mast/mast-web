import path from "path";
import fs from "fs";
import Cookies from "cookies"
import url_prefix from "@/utils/server/url_prefix";

export default async (req, res) => {

    let { unit } = req.query // Should be of the form CQV-Perspectives?version=1
    let version = req.query.version
    let solutions = req.query.solutions
    let token = req.query.token
    let tokenType

    if (!version || isNaN(version)) {
        return res.status(400).send("Version must be a positive integer.");
    }

    if (!token) {
        const cookies = new Cookies(req, res, { secure: true })
        token = cookies.get("session")
        tokenType = "session"
    } else {
        tokenType = "PAT"
    }

    const authRes = await fetch(`${url_prefix}/api/auth/token`, {
        method: "POST",
        body: JSON.stringify({
            token,
            tokenType
        })
    })

    if (authRes.status >= 300) {
        return res.status(401).send("Failed to authenticate.")
    }

    const user = JSON.parse(await authRes.text()).user

    if (user.power < 3) {
        return res.status(403).send("Your power is not high enough to access units.")
    }

    if (user.power === 3 && (!user.data.units || !user.data.units[`${unit}-${version}`] || ['open', 'submitted', 'checked'].indexOf(user.data.units[`${unit}-${version}`].status) === -1)) {
        return res.status(401).send("You do not have access to this unit.")
    }

    let fileName;

    if (solutions !== null && typeof solutions !== 'undefined') {
        fileName = `S-${unit}-${version}.pdf`;
    } else {
        fileName = `${unit}-${version}.pdf`;
    }

    const filePath = path.join(process.env.RESOURCE_DIR,
        "content",
        "units",
        `${unit}`,
        fileName);

    try {

        const pdfBuffer = fs.readFileSync(filePath)

        res.setHeader('Content-Disposition', `attachment; filename="${fileName}"`)
        res.setHeader('Content-Type', 'application/pdf')
        res.send(pdfBuffer);
    } catch (err) {
        console.log(err)
        res.status(404).send("File could not be found.")
    }
}
