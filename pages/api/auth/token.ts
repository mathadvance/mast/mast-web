import { mastDB } from "@/utils/server/mongodb";
import { redis_PATs, redis_sessionIDs } from "@/utils/server/redis";
import keygen from "@/utils/server/keygen";

// Checks validity of tokens
export default async (req, res) => {
  const request = JSON.parse(req.body);

  // checks that body has EVERYTHING needed

  if (!request.token) {
    return res
      .status(400)
      .send(`Either an empty token was sent or no token was sent.`);
  }

  if (!request.tokenType) {
    return res
      .status(400)
      .send(
        `The "tokenType" property of the request body must be a non-empty string.`
      );
  }

  if (request.tokenType === "session") {
    // This is for the website itself
    // Stored in browser cookies, not very secure so it has a limited lifetime
    const redisJSON = await redis_sessionIDs.get(request.token);
    if (!redisJSON) {
      return res.status(401).send("Invalid session token sent.");
      // It's 400 because you're sending an invalid sessionID
      // which should not happen, so it's an error
    }
    const redisObject = JSON.parse(redisJSON);
    const user = await mastDB.collection("users").findOne(
      { username: { $eq: redisObject.username } },
      { projection: { _id: 0, destructionDate: 0, hashedPassword: 0, PATs: 0 } } // projection filters out stuff
    );

    // timestamp checks; regenerate cookies if appropriate
    // also logout (i.e. delete session on client and serverside)
    // if earliest_acceptable_session_timestamp is greater than
    // timestamp for this current session ID.
    if (
      redisObject.timestamp <
      user.Timestamps.earliest_acceptable_session_timestamp
    ) {
      return res.status(401).send("Token is no longer valid.");
    }
    if (
      Date.now() > redisObject.lastRegenerated + 1000 * 60 * 60 &&
      redisObject.regenerate
    ) {
      // if it's an hour past when we set the token,
      // AND it regenerates (which should always be true??) refresh it
      const session = keygen();

      const redisJSON = JSON.stringify({
        username: user.username,
        timestamp: redisObject.timestmap,
        regenerate: true,
        lastRegenerated: Date.now(),
      });

      redis_sessionIDs.set(session, redisJSON, "EX", 60 * 60 * 24 * 30);
      return res.status(200).send(
        JSON.stringify({
          user,
          newSession: session,
        })
      );
    }
    // otherwise, don't refresh token
    return res.status(200).send(JSON.stringify({ user }));
  }

  if (request.tokenType === "PAT") {
    // supposed to be secure, so it lasts a while (depending on how long you make your PATs last)
    const redisJSON = await redis_PATs.get(request.token);
    if (!redisJSON) {
      return res.status(401).send("Invalid PAT sent.");
      // It's 400 because you're sending an invalid PAT
      // which should not happen, so it's an error
    }
    const redisObject = JSON.parse(redisJSON);
    const user = await mastDB.collection("users").findOne(
      { username: { $eq: redisObject.username } },
      { projection: { _id: 0, destructionDate: 0, hashedPassword: 0, PATs: 0 } } // projection filters out the hashed password
    );
    return res.status(200).send(JSON.stringify({ user }));
  }
};
