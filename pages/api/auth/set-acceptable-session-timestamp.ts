import { mastDB } from "@/utils/server/mongodb";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";

export default async (req, res) => {
  const request = JSON.parse(req.body);
  let token;
  const tokenType = request.tokenType;
  if (tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
    cookies.set("session");
  } else if (tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid token type sent.");
  }
  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType,
    }),
  });
  if (authRes.status >= 300) {
    return res.status(401).send(`Invalid ${request.tokenType} token sent.`);
  }

  const user = JSON.parse(await authRes.text());

  mastDB.collection("users").updateOne(
    { username: user.username },
    {
      $set: {
        "Timestamps.earliest_acceptable_session_timestamp": Date.now(),
      },
    }
  );
  return res
    .status(200)
    .send("Set earliest acceptable authentication timestamp.");
};
