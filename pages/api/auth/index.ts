import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";

// This is the BROWSER-ONLY api route (cookie stuff) that basically redirects all verifying to check-token

export default async (req, res) => {
  const cookies = new Cookies(req, res, { secure: true });
  const token = cookies.get("session");

  const result = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType: "session",
    }),
  });

  if (result.status < 300) {
    const resText = await result.text();
    if (resText) {
      const resObject = JSON.parse(resText);
      if (resObject.newSession) {
        cookies.set("session", resObject.newSession, {
          maxAge: 1000 * 60 * 60 * 24 * 30,
          path: "/",
          httpOnly: true,
          sameSite: "strict",
          secure: process.env.NODE_ENV !== "development",
        });
      }
      return res.status(200).send(JSON.stringify(resObject.user));
    } else {
      return res.status(401).send("Invalid session cookie sent.");
    }
  }
  return res.status(200).send(JSON.stringify(null));
};
