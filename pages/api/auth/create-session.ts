import Cookies from "cookies";
import keygen from "@/utils/server/keygen";
import { redis_sessionIDs } from "@/utils/server/redis";
import url_prefix from "@/utils/server/url_prefix";

// creates session tokens and personal access tokens

export default async (req, res) => {
  const request = JSON.parse(req.body);
  const login = await fetch(`${url_prefix}/api/auth/check-credentials`, {
    method: "POST",
    body: JSON.stringify({
      username: request.username,
      password: request.password,
    }),
  });
  if (login.status < 300) {

    const username = await login.text()

    let maxAge: number;
    if (request.rememberMe) {
      maxAge = 60 * 60 * 24 * 30;
      // We regenerate tokens if request.rememberMe is true,
      // so if a user doesn't visit the website through the same computer in a month,
      // it's probably not too much of an inconvenience for them to relog.
    } else {
      maxAge = 60 * 60;
    }

    const token = keygen();

    const redisValString = JSON.stringify({
      username,
      timestamp: Date.now(),
      regenerate: request.rememberMe,
      lastRegenerated: Date.now(),
    });

    const cookies = new Cookies(req, res, { secure: true });

    cookies.set("session", token, {
      maxAge: 1000 * maxAge,
      path: "/",
      httpOnly: true,
      sameSite: "strict",
      secure: process.env.NODE_ENV !== "development",
    });

    redis_sessionIDs.set(token, redisValString, "EX", maxAge);

    return res.status(200).send(token);
  } else {
    return res.status(login.status).send(await login.text());
  }
};
