import { mastDB } from "@/utils/server/mongodb";
import { ip_rate_limit } from "@/utils/server/ip-rate-limit";
import argon2 from "argon2";
import EmailValidator from "email-validator";

// Just check credentials for session creation, password/email changing, what have you.

export default async (req, res) => {
  let ip = req.headers["x-real-ip"] || req.connection.remoteAddress;
  let result = await ip_rate_limit(ip);
  if (result) {
    return res.status(429).send("You're sending too many login requests; the limit is 10 requests every minute.");
  }

  const request = JSON.parse(req.body);

  if (EmailValidator.validate(request.username)) {
    const email_proxy = await mastDB
      .collection("email_proxy")
      .findOne({ email: { $eq: request.username } });
    if (email_proxy) {
      request.username = email_proxy.username;
    } else {
      return res
        .status(404)
        .send("There is no user associated with this email.");
    }
  }
  const user = await mastDB.collection("users").findOne({
    username: { $eq: request.username },
  });
  if (!user) {
    return res
      .status(404)
      .send("There is no user associated with this username.");
  }
  if (await argon2.verify(user.hashedPassword, request.password)) {
    return res.status(200).send(request.username);
  } else {
    return res.status(401).send("Password is incorrect.");
  }
};
