import Cookies from "cookies";
import url_prefix from "@/utils/server/url_prefix";
import { mastDB } from "@/utils/server/mongodb";
import { redis_PATs } from "@/utils/server/redis";

export default async (req, res) => {
  const request = JSON.parse(req.body);
  let token;
  if (request.tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (request.tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid token type sent.");
  }
  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType: request.tokenType,
    }),
  });
  if (authRes.status >= 300) {
    return res.status(400).send("Invalid token sent.");
  }
  const username = JSON.parse(await authRes.text()).user.username;
  const user = await mastDB.collection("users").findOne({ username });
  const names = {};
  for (const prop in user.PATs) {
    const redisJSON = await redis_PATs.get(user.PATs[prop].value);
    if (!redisJSON) {
      const obj = {};
      obj[`PATs.${prop}`] = "";
      mastDB.collection("users").updateOne({ username }, { $unset: obj });
    } else {
      names[prop] = { expiration_date: user.PATs[prop].expiration_date };
    }
  }
  return res.status(200).send(JSON.stringify(names));
};
