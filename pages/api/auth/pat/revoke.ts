import { mastDB } from "@/utils/server/mongodb";
import { redis_PATs } from "@/utils/server/redis";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";

export default async (req, res) => {
  // req:
  //  token & tokenType are for auth
  //  name is the name of the PAT we're revoking
  const request = JSON.parse(req.body);
  let token;
  if (request.tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (request.tokenType === "PAT") {
    token = request.token;
  } else {
    return res.status(400).send("Invalid 'tokenType'.");
  }
  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType: request.tokenType,
    }),
  });
  if (authRes.status >= 300) {
    return res.status(400).send("Invalid 'token'.");
  }
  const username = JSON.parse(await authRes.text()).user.username;
  const user = await mastDB
    .collection("users")
    .findOne({ username }, { projection: { _id: 0, destructionDate: 0, hashedPassword: 0, PATs: 0 } });
  if (user.PATs[request.name]) {
    redis_PATs.del(user.PATs[request.name].value);
    return res.status(200).send(`PAT '${request.name}' revoked.`);
  } else {
    return res.status(400).send(`PAT '${request.name}' does not exist.`);
  }
};
