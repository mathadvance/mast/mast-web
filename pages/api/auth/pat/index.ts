import { redis_PATs } from "@/utils/server/redis";
import url_prefix from "@/utils/server/url_prefix";
import Cookies from "cookies";
import keygen from "@/utils/server/keygen";
import { mastDB } from "@/utils/server/mongodb";

export default async (req, res) => {
  const { name, lifetime, tokenType } = req.body;
  if (!name) {
    return res.status(400).send(`Must set 'name' property`);
  }
  if (!lifetime) {
    return res.status(400).send(`Must set 'lifetime' property`);
  }
  if (lifetime < 0 || lifetime > 1000) {
    return res
      .status(400)
      .send(`The PAT lifetime must be a number between 1 and 1000 inclusive.`);
  }

  let { token } = req.body;
  if (tokenType === "session") {
    const cookies = new Cookies(req, res, { secure: true });
    token = cookies.get("session");
  } else if (tokenType != "PAT") {
    return res.status(400).send("Invalid token type sent.");
  }

  const authRes = await fetch(`${url_prefix}/api/auth/token`, {
    method: "POST",
    body: JSON.stringify({
      token,
      tokenType: tokenType,
    }),
  });

  if (authRes.status >= 300) {
    return res.status(400).send("Invalid session token sent.");
  }
  const user = JSON.parse(await authRes.text()).user;
  if (user.PATs && user.PATs[name]) {
    return res.status(400).send("PAT name already taken.");
  }

  const PAT = keygen();

  const PATObj = {};
  PATObj[`PATs.${name}`] = {
    value: PAT,
    expiration_date: Date.now() + 1000 * 60 * 60 * 24 * lifetime, // Just so we can show this on PAT page
  };

  // Set mastDB as well so we can display PAT names on browser
  // (want to be able to lookup PATs by user)

  mastDB.collection("users").updateOne(
    {
      username: user.username,
    },
    {
      $set: PATObj,
    }
  );

  redis_PATs.set(
    PAT,
    JSON.stringify({
      name: name,
      username: user.username,
    }),
    "EX",
    60 * 60 * 24 * lifetime // lifetime is in days
  );

  return res.status(200).send(PAT);
};
