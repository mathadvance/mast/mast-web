import RadioGroup from "@/components/Headless/RadioGroup";

import { useTheme } from "@/contexts/ThemeProvider";

import { FormButton } from "@/components/FormComponents";
import { useToast } from "@/contexts/ToastProvider";
import { useAuth } from "@/contexts/AuthProvider";
import { useState } from "react";

export default function Settings() {
  const { theme, setTheme, sideBarColor, setSideBarColor } = useTheme();
  const [emails, setEmails] = useState(useAuth().user.Settings.emails || "yes");
  const { showToast } = useToast();

  return (
    <>
      <h1>Settings</h1>
      <p>
        Persist your settings if you want them to be loaded the next time you
        visit the site.
      </p>
      <RadioGroup
        label={"Theme"}
        value={theme}
        onChange={setTheme}
        options={[
          {
            value: "light",
            label: "Light Theme",
          },
          {
            value: "dark",
            label: "Dark Theme",
          },
          {
            value: "browser",
            label: "Browser Theme",
            desc: "This is determined through the prefers-color-scheme media query.",
          },
        ]}
      />
      <RadioGroup
        label={"Navbar Color"}
        value={sideBarColor}
        onChange={setSideBarColor}
        options={[
          {
            value: "pink",
            label: "Pink",
          },
          {
            value: "blue",
            label: "Blue",
          },
        ]}
      />
			<RadioGroup
				label={"Emails"}
				value={emails}
				onChange={setEmails}
				options={[
					{
						value: "yes",
						label: "Send me MAST-related emails.",
            desc: "Emails include lecture information and new units."
					},
					{
						value: "no",
						label: "Do not send me MAST-related emails."
					}
				]}
			/>
      <FormButton
        text="Persist Settings"
        onClick={() => {
          showToast({ text: "Settings persisted.", type: "success" })
          fetch("/api/profile/settings", {
            method: "POST",
            body: JSON.stringify({
              tokenType: "session",
              Settings: {
                theme_preference: theme,
                sidebar_color: sideBarColor,
                emails,
              },
            }),
          });
        }}
      />
    </>
  );
}
