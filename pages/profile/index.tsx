import { FormInput, FormButton } from "@/components/FormComponents";
import { useState } from "react";
import { useAuth } from "@/contexts/AuthProvider";
import router from "next/router";
import EmailValidator from "email-validator";
import { useToast } from "@/contexts/ToastProvider";

export default function Profile() {
  const { user, setUser } = useAuth();
  const { showToast } = useToast();

  const [newEmail, setNewEmail] = useState("");

  const [currentPassword, setCurrentPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");

  async function changeEmail() {
    if (!EmailValidator.validate(newEmail)) {
      showToast({type:"error",text:"You must input a valid email."});
      return;
    }

    const res = await fetch("/api/email/change-email", {
      method: "POST",
      body: JSON.stringify({
        tokenType: "session",
        oldEmail: user.email,
        newEmail,
        action: "send",
      }),
    });

    if (res.status < 300) {
      setNewEmail("");
      return
    } else {
      showToast({type:"error",text:await res.text()})
      return
    }
  }

  function changePassword() {
    if (currentPassword.length < 8) {
      showToast({type:"error",text:"The current password you entered is incorrect."}); // by definition, all passwords must be >= 8 chars, so if it's not then it's wrong
      return;
    }
    (async () => {
      const res = await fetch("/api/auth/check-credentials", {
        method: "POST",
        body: JSON.stringify({
          username: user.username,
          password: currentPassword,
        }),
      });
      if (res.status >= 300) {
        return showToast({type:"error",text:"The current password you entered is incorrect."});
      }
      if (newPassword.length < 8) {
        return showToast({type:"error",text:"Your new password must be 8 characters or longer."});
      }
      if (newPassword === currentPassword) {
        return showToast({type:"error",text:"Your new password must be different from your old password."});
      }
      await fetch("/api/profile/change-password", {
        method: "POST",
        body: JSON.stringify({
          tokenType: "session",
          password: newPassword,
        }),
      });
      DeleteAllSessions();
      return;
    })();
    return;
  }

  function DeleteAllSessions() {
    fetch("/api/auth/set-acceptable-session-timestamp", {
      method: "POST",
      body: JSON.stringify({
        tokenType: "session",
      }),
    });
    setUser(undefined);
    router.push("/about");
    return;
  }

  return (
    <>
      <h1>Profile</h1>
      <h2>Change Email</h2>
      <p>
        Your account's current email address is <em>{user.email}</em>.
      </p>
      <p>
        Clicking the "change email" button will send a confirmation email to
        the new email.
      </p>
      <div className="space-y-4 pt-2">
        <FormInput
          placeholder="New Email"
          value={newEmail}
          onChange={(event) => {
            setNewEmail(event.target.value);
          }}
        />
        <FormButton
          text="Change Email"
          onClick={() => {
            changeEmail();
          }}
        />
      </div>
      <h2>Change Password</h2>
      <p>Input your current and new password in the fields below.</p>
      <p>Changing your password will sign you out of all sessions.</p>
      <div className="space-y-4 pt-2">
        <FormInput
          type="password"
          placeholder="Current Password"
          onChange={(event) => {
            setCurrentPassword(event.target.value);
          }}
        />
        <FormInput
          type="password"
          placeholder="New Password"
          onChange={(event) => {
            setNewPassword(event.target.value);
          }}
        />
        <FormButton
          text="Change Password"
          onClick={() => {
            changePassword();
          }}
        />
      </div>
      <h2>Sign Out of All Sessions</h2>
      <p>
        Clicking the button below will sign you out of all sessions,{" "}
        <em>including this one</em>.
      </p>
      <div className="pt-2 space-y-4">
        <FormButton
          text="Sign Out of All Sessions"
          onClick={() => {
            DeleteAllSessions();
          }}
        />
      </div>
    </>
  );
}
