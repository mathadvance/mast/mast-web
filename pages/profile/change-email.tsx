import { useAuth } from "@/contexts/AuthProvider";
import url_prefix from "@/utils/server/url_prefix";

export default function change_email({ verificationSucceeded, email }) {
  const { user, setUser } = useAuth();

  if (verificationSucceeded) {
    user.email = email
    setUser(user);
    return (
      <>
        <h1>Email Successfully Changed</h1>
        <p>
          Head over to <a href="/home">your homepage</a>.
        </p>
      </>
    );
  }

  return (
    <>
      <h1>Email Validation Error</h1>
      <p>
        The verification key is either malformed, expired, or already has been
        used.
      </p>
    </>
  );
}

export async function getServerSideProps(context) {
  const key = context.query.key
  const res = await fetch(`${url_prefix}/api/email/change-email`, {
    method: "POST",
    body: JSON.stringify({
      action: "verify",
      key,
    }),
  })
  if (res.status < 300) {
    const { email } = await res.json()
    return {
      props: {
        verificationSucceeded: true,
        email
      }
    }
  } else {
    return {
      props: {
        verificationSucceeded: false,
      }
    }
  }
}