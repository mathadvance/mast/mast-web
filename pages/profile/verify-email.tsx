import { useAuth } from "@/contexts/AuthProvider";
import url_prefix from "@/utils/server/url_prefix";

export default function verify_email({ verificationSucceeded }) {
  const { user, setUser } = useAuth();

  if (verificationSucceeded) {
    user.power = 1
    setUser(user);
    return (
      <>
        <h1>Email Verified</h1>
        <p>
          Head over to <a href="/home">your homepage</a>.
        </p>
      </>
    );
  }

  return (
    <>
      <h1>Email Validation Error</h1>
      <p>
        The verification key is either malformed, expired, or already has been
        used.
      </p>
    </>
  );
}

export async function getServerSideProps(context) {
  const key = context.query.key
  const res = await fetch(`${url_prefix}/api/email/verify`, {
    method: "POST",
    body: key,
  })
  if (res.status < 300) {
    return {
      props: {
        verificationSucceeded: true
      }
    }
  } else {
    return {
      props: {
        verificationSucceeded: false
      }
    }
  }
}