import { FormButton, FormFileUpload, FormTextArea } from "@/components/FormComponents";
import { useAuth } from "@/contexts/AuthProvider"
import { useToast } from "@/contexts/ToastProvider";
import { useState } from "react";

export default function Bio() {
    const { user } = useAuth();
    const { showToast } = useToast();

    const [text, setText] = useState((user && user.data.bio && user.data.bio.text) ? user.data.bio.text : "");
    const [pic, setPic] = useState(undefined);

    return <>
        <h1>Staff Bio</h1>
        <p>Updates will take some time to propagate to the public staff page.</p>

        <h2>Update Text</h2>
        <div className="space-y-4 pt-2">
            <FormTextArea
                defaultValue={text}
                onChange={(event) => {
                    setText(event.target.value)
                }}
            />
            <FormButton
                text="Update"
                onClick={() => {
                    fetch("/api/upload/bio/text", {
                        method: "POST",
                        body: JSON.stringify({
                            tokenType: "session",
                            text
                        })
                    })
                    showToast({ type: "success", text: "Staff bio updated." })
                }}
            />
        </div>
        <h2>Upload Staff Photo</h2>
        <p>The image must be a JPEG, PNG, or GIF. There is a filesize limit of 5 MB.</p>
        <FormFileUpload
            text="Photo"
            accept="image/png, image/gif, image/jpeg"
            onChange={(event) => {
                setPic(event.target.files[0]);
            }}
        />
        <FormButton text="Upload"
            onClick={() => {
                (async () => {
                    const formData = new FormData();
                    formData.append("tokenType", "session");
                    formData.append("pic", pic);
                    const res = await fetch("/api/upload/bio/image", {
                        method: "POST",
                        body: formData
                    })
                    if (res.status >= 300) {
                        showToast({ type: "error", text: await res.text() })
                    } else {
                        showToast({ type: "success", text: "Staff photo updated." })
                    }
                })();
            }} />
    </>
}