import { FormButton } from "@/components/FormComponents";
import router from "next/router";
import { useEffect, useState } from "react";
import NextLink from "next/link";

function TokenView({
  names,
  onClick = function (name) { },
}: {
  names?: any;
  onClick?: any;
}) {
  const PATs = [];
  for (const prop in names) {
    PATs.push({ name: prop, expiration_date: names[prop].expiration_date });
  }

  if (PATs.length === 0) {
    return (
      <p>
        You don't have any tokens yet.{" "}
        <NextLink href="/profile/pat/generate">
          <a className="blue-link">Generate one?</a>
        </NextLink>
      </p>
    );
  }

  function RowItem({ children }) {
    return <div className="w-32 flex items-center">{children}</div>
  }

  return (
    <div className="rounded-md ring-1 ring-gray-200 dark:ring-gray-700 divide-y divide-solid divide-gray-200 dark:divide-gray-700 my-4">
      <div className="px-4 py-3 dark:bg-gray-900 flex flex-row justify-between rounded-t-md">
        <RowItem>Name</RowItem>
        <RowItem>Expiration Date</RowItem>
        <div className="w-32 flex justify-center items-center">Action</div>
      </div>
      {PATs.map((PAT, index) => {
        return (
          <div
            className={`px-4 py-3 dark:bg-gray-900 flex flex-row justify-between
            ${index === PATs.length - 1 ? 'rounded-b-md' : ''}`}
            key={index}
          >
            <RowItem>{PAT.name}</RowItem>
            <RowItem>
              {
                new Date(PAT.expiration_date).toLocaleString('default', { year: "numeric", month: "short", day: "numeric" })
              }
            </RowItem>
            <div className="w-32 flex justify-center items-center">
              <div className="w-20">
                <FormButton
                  text="Revoke"
                  onClick={() => {
                    onClick(PAT.name);
                  }}
                  color="bg-red-700 hover:bg-red-600 active:bg-red-800"
                />
              </div>
            </div>
          </div>
        );
      })}
    </div >
  );
}


export default function PAT() {
  const [loading, setLoading] = useState(true);
  const [names, setNames] = useState({});
  useEffect(() => {
    (async () => {
      const res = await fetch("/api/auth/pat/names", {
        method: "POST",
        body: JSON.stringify({
          tokenType: "session",
        }),
      });
      if (res.status < 300) {
        setNames(JSON.parse(await res.text()));
      } else {
        return router.push("/about");
      }
      setLoading(false);
    })();
  }, []);
  function Revoke(name, unset) {
    if (unset) {
      delete names[name];
      setNames({ ...names }); // Hacky way to force rerender
    }
    fetch("/api/auth/pat/revoke", {
      method: "POST",
      body: JSON.stringify({
        tokenType: "session",
        name,
      }),
    });
  }

  function Tokens() {
    return (
      <TokenView
        names={names}
        onClick={function (name) {
          Revoke(name, true);
        }}
      />
    );
  }

  return (
    <>
      <h1>Personal Access Tokens</h1>
      <h2>Existing Tokens</h2>
      <div>
        <Tokens />
      </div>
      <h2>Revoke All Tokens</h2>
      <p>
        Clicking the button below will revoke all your personal access tokens.
      </p>
      <div className="pt-2">
        <FormButton
          text="Revoke all PATs"
          color="bg-red-700 hover:bg-red-600 active:bg-red-800"
          onClick={() => {
            for (const prop in names) {
              Revoke(prop, false);
            }
            setNames({});
          }}
        />
      </div>
    </>
  );
}
