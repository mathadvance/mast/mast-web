import { useState } from "react";
import { FormButton,  FormInput } from "@/components/FormComponents";
import { useAuth } from "@/contexts/AuthProvider";
import { useToast } from "@/contexts/ToastProvider";

export default function GeneratePAT() {
  const [PAT, setPAT] = useState("");
  const [PATName, setPATName] = useState("");
  const [PATLifetime, setPATLifetime] = useState("");
  
  const { user } = useAuth();
  const { showToast } = useToast();

  async function CreatePAT() {
    if (!PATName) {
      return showToast({type: "error", text: "A PAT name must be set."});
    }
    if (!PATLifetime) {
      return showToast({type: "error", text: "A PAT lifetime must be set."});
    }
    if (user.PATs && user.PATs[PATName]) {
      return showToast({type: "error", text: "The PAT name has already been taken."});
    }
    const res = await fetch("/api/auth/pat", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenType: "session",
        name: PATName,
        lifetime: PATLifetime,
      }),
    });

    if (res.status < 300) {
      showToast({type: "success", text: "PAT created."})
      return setPAT(await res.text());
    } else {
      return showToast({type: "error", text: await res.text()});
    }
  }

  return (
    <>
      <h1>Create New Personal Access Token</h1>
      {PAT ? (
        <>
          <p>
            Your personal access token is <em>{PAT}</em>.
          </p>
          <p>
            All API routes (besides those that modify account details, like
            email address and password) accept it as a form of authentication,
            so be sure to keep it safe and only give it to programs you trust.
          </p>
          <p>
            Make sure you store the token somewhere safe, because after you
            leave this page, you will not be able to access it again.
          </p>
        </>
      ) : (
        <>
          <p>
            For other apps that use the MAST API, you will need to use a
            personal access token in order to authenticate.
          </p>
          <div className="pt-2 space-y-4">
            <div className="grid grid-cols-2 gap-x-4">
              <FormInput
                placeholder="PAT Name"
                onChange={(event) => {
                  setPATName(event.target.value);
                }}
              />
              <FormInput
                placeholder="Token Lifetime (in days)"
                value={PATLifetime}
                pattern="\d{0,3}"
                onChange={(event) => {
                  setPATLifetime((PATLifetime) =>
                    event.target.validity.valid ? event.target.value : PATLifetime
                  );
                }}
              />
            </div>
            <FormButton
              text="Create New PAT"
              onClick={() => {
                CreatePAT();
              }}
            />
          </div>
        </>
      )}
    </>
  );
}
