import NextLink from "next/link";
import url_prefix from "@/utils/server/url_prefix";
import { useAuth } from "@/contexts/AuthProvider";

export default function Catalog({ units }) {

    const { user } = useAuth();

    function Units({ subject }) {
        function SubUnits({ difficulty }) {
            return <>
                {Object.keys(units).map((key) => { // We don't need to sort by relevance because fs maps alphabetically :)
                    const unit = units[key]
                    if (unit.code.charAt(0) === subject && unit.code.charAt(1) === difficulty)
                        return <div key={key}><p><em>{unit.name} ({unit.code})</em> - {unit.desc}</p>
                            <p className="text-sm">Versions: {unit.versions.map((version) => {
                                if (user && user.power >= 4)
                                    return <span><a href={`/api/files/units/${unit.code}-${key}?version=${version}`} className="blue-link">{`${unit.code}-${version}`}</a><span>{" "}</span><a href={`/api/files/units/${unit.code}-${key}?version=${version}&solutions`} className="pink-link">(Solutions)</a></span>
                                else
                                    return `${unit.code}-${version}`
                            }).reduce((prev, curr) => [prev, " ", curr])}</p></div>
                    else
                        return null;
                })}</>
        }
        return <>
            <h3>Palatable</h3>
            <SubUnits difficulty="P" />
            <h3>Quotidian</h3>
            <SubUnits difficulty="Q" />
            <h3>Ruthless</h3>
            <SubUnits difficulty="R" />
        </>
    }

    return <>
        <h1>Unit Catalog</h1>
        <p>For an explanation of the unit codes, see the <NextLink href="/resources/unit-guide"><a className="blue-link">unit guide.</a></NextLink></p>
        <h2>Algebra</h2>
        <Units subject="A" />
        <h2>Combinatorics</h2>
        <Units subject="C" />
        <h2>Geometry</h2>
        <Units subject="G" />
        <h2>Number Theory</h2>
        <Units subject="N" />
        <h2>Miscellaneous</h2>
        <Units subject="M" />
    </>
}

export async function getStaticProps(context) {

    const res = await fetch(`${url_prefix}/api/public/units`, { method: "GET" })

    if (res.status >= 300) {
        return {
            props: {
                units: {}
            },
            revalidate: 120
        }
    }

    const units = await res.json()

    return {
        props: {
            units
        },
        revalidate: 120
    }
}