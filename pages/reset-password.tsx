import {
  FormBox,
  FormInput,
  FormButton,
  
} from "@/components/FormComponents";
import { useToast } from "@/contexts/ToastProvider";
import url_prefix from "@/utils/server/url_prefix";
import { useState } from "react";

export default function reset_password(props) {
  const [password, setPassword] = useState("");
  const { showToast } = useToast();
  
  const [resetted, setResetted] = useState(false);
  const [verificationSucceeded, setVerificationSucceeded] = useState(props.verificationSucceeded);

  function reset_password() {
    if (password.length < 8) {
      showToast({type: "error", text: "Your new password must be 8 characters or longer."});
      return;
    }
    (async () => {
      const res = await fetch("/api/email/reset-password", {
        method: "POST",
        body: JSON.stringify({
          action: "change",
          key: props.keyStr,
          password,
        }),
      });
      if (res.status < 300) {
        setResetted(true);
        
        return;
      } else {
        setVerificationSucceeded(false);
        return;
      }
    })();
  }

  if (verificationSucceeded) {
    return (
      <FormBox>
        <h1>Reset Password</h1>
        {resetted ? (
          <>
            <p>Your password has been reset.</p>
          </>
        ) : (
          <>
            <FormInput
              placeholder="New Password"
              type="password"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
            <FormButton
              text="Reset Password"
              onClick={() => {
                reset_password();
              }}
            />
          </>
        )}
      </FormBox>
    );
  }

  return (
    <FormBox>
      <h1>Reset Password</h1>
      <p>
        The password reset key is either malformed, expired, or already has been
        used.
      </p>
    </FormBox>
  );
}

export async function getServerSideProps(context) {
  const key = context.query.key
  const res = await fetch(`${url_prefix}/api/email/reset-password`, {
    method: "POST",
    body: JSON.stringify({
      action: "verify",
      key,
    }),
  });
  if (res.status < 300) {

    return {
      props: {
        verificationSucceeded: true,
        keyStr: key
      }
    }

  } else {
    return {
      props: {
        verificationSucceeded: false,
        keyStr: key
      }
    }
  }
}