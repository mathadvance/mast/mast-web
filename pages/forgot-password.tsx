import {
  FormBox,
  FormInput,
  FormButton,
} from "@/components/FormComponents";
import { useToast } from "@/contexts/ToastProvider";
import { useState } from "react";

export default function forgot_password() {
  const { showToast } = useToast();

  const [resetted, setResetted] = useState(false);
  const [input, setInput] = useState("");
  async function send_reset_link() {
    const res = await fetch("/api/email/forgot-password", {
      method: "POST",
      body: input,
    });
    if (res.status < 300) {
      setResetted(true);
      showToast({type:"success",text:"Reset link sent."})
    } else {
      showToast({type: "error", text: await res.text()});
    }
    return;
  }
  return (
    <FormBox>
      <h1 className="font-normal">Forgot Password</h1>
      {!resetted ? (
        <>
          <FormInput
            placeholder="Email/Username"
            desc="If you forgot your username, it will be emailed to you along with the password reset link."
            onChange={(event) => {
              setInput(event.target.value);
            }}
          />
          <FormButton
            text="Submit"
            onClick={() => {
              send_reset_link();
            }}
          />
        </>
      ) : (
        <p>The password reset email has been sent.</p>
      )}
    </FormBox>
  );
}
