export default function Lectures({ appArray }) {
    return <>
        <h1>MAST Lecture Archive</h1>
        <p>The slides for all previous MAST lectures will be stored here.</p>
        <ul className="list-disc list-inside">
				    <li><a className="blue-link" href="/api/files/lectures/Posets">Posets</a> - May 22, 2022</li>
            <li><a className="blue-link" href="https://docs.google.com/presentation/d/1ekrSB8FSoNfJMeHcvT1D_ibKVKFgh1EFgpZnR-pIsPo/edit#slide=id.g10dbb351a9e_1_21">Projective Geometry</a> - May 15, 2022</li>
            <li><a className="blue-link" href="/api/files/lectures/PIE">PIE</a> - April 24, 2022</li>
            <li><a className="blue-link" href="/api/files/lectures/Telescoping">Telescoping</a> - April 3, 2022</li>
        </ul>
    </>;
}
