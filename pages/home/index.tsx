import { useAuth } from "@/contexts/AuthProvider";
import { powerToRole } from "@/utils/server/powerToRole";
import { FormButton, FormFileUpload } from "@/components/FormComponents";
import NextLink from "next/link";
import { useToast } from "@/contexts/ToastProvider";
import url_prefix from "@/utils/server/url_prefix";

import ListBox from "@/components/Headless/ListBox";
import CheckBox from "@/components/CheckBox";

import { useState } from "react";

import _ from "lodash";

export default function Home({ units }) {
  const { user, setUser } = useAuth();
  const { showToast } = useToast();

  const roleName = powerToRole(user.power);

  function Unverified() {
    function send_verification_email() {
      showToast({ type: "success", text: "Verification email resent." })
      fetch("/api/email/send-verification", {
        method: "POST",
        body: JSON.stringify({
          tokenType: "session",
        }),
      });
    }

    return (
      <>
        <p>
          Please verify your account through your email address{" "}
          <em>({user.email})</em>. If the verification email did not send or the
          link has expired, you can resend it.
        </p>
        <p>
          If your account is not verified in fourteen days, it will be
          automatically deleted.
        </p>
        <div className="pt-2">
          <FormButton
            text="Resend Verification Email"
            onClick={() => {
              send_verification_email();
            }}
          />
        </div>
      </>
    );
  }

  function User() {
    function become_applicant() {
      showToast({ type: "success", text: "You are now an applicant." })
      fetch("/api/power/applicant", {
        method: "POST",
        body: JSON.stringify({ tokenType: "session" }),
      });
      user.power = 2;
      setUser({ ...user }); // Forces rerender of RenderRole
    }
    return (
      <>
        <p>
          If you just signed up to check out the website or aren't sure about
          applying yet, you should remain a user.
        </p>
        <p>
          Otherwise, click the button below to become a late applicant and get
          access to the application portal.
        </p>
		<p>
			<em>Please only do so if you have qualified for the AIME.</em>
		</p>
        <p>
          <em>This operation is irreversible</em>, though you are not obligated
          if you do click the button. It's just so I get a better idea of who's
          planning to apply.
        </p>
        <div className="pt-2">
          <FormButton
            text="Become an Applicant"
            onClick={() => {
              become_applicant();
            }}
          />
        </div>
      </>
    );
  }

  function Applicant() {
    if (user.data.applied) {
      return (
        <>
          <p>
            Congratulations on applying for MAST! The staff team will get back
            to you regarding your application.
          </p>
        </>
      );
    }

    return (
      <>
        <p>
          Follow the{" "}
          <NextLink href="/apply">
            <a className="blue-link">application instructions</a>
          </NextLink>{" "}
          and enter the{" "}
          <NextLink href="/home/app-portal">
            <a className="blue-link">application portal</a>
          </NextLink>{" "}
          to begin applying.
        </p>
        <div className="pt-2">
          <NextLink href="/home/app-portal">
            <a>
              <FormButton text="Go to Application Portal" />
            </a>
          </NextLink>
        </div>
      </>
    );
  }

  function Student() {
    const [unit, setUnit] = useState(undefined);
    const [version, setVersion] = useState(undefined);
    const [action, setAction] = useState(undefined);
    const [PDF, setPDF] = useState(undefined);

    const [filter, setFilter] = useState(false);

    const { showToast } = useToast();

    const options = {};
    for (const key in units) {
      options[key] = `(${units[key].code}) ${units[key].name}`
    }

    function unitStatus() {
      const currentUnit = units[unit] ? `${units[unit].code}-${unit}-${version}` : undefined
      if (!user.data.units || !user.data.units[currentUnit]) {
        return 'none';
      } else {
        return user.data.units[currentUnit].status
      }
    }

    function updateUnit(newUnit) {
      if (unit != newUnit) {
        setUnit(newUnit)
        setVersion(undefined)
        setAction(undefined)
      }
    }

    function updateVersion(newVersion) {
      if (version != newVersion) {
        setVersion(newVersion)
        setAction(undefined)
        setPDF(undefined)
      }
    }

    function updateAction(newAction) {
      if (action != newAction) {
        setAction(newAction)
        setPDF(undefined)
      }
    }

    async function Action() {
      if (!unit || !version || !action) {
        return showToast({ type: 'error', text: 'A unit, version, and action must be selected.' });
      }
      if (action === 'submit' && !PDF) {
        return showToast({ type: 'error', text: 'You must upload a file to submit.' })
      }
      if (action === 'submit') {
        const formData = new FormData();
        formData.append('tokenType', 'session');
        formData.append('unit', `${units[unit].code}-${unit}-${version}`)
        formData.append('PDF', PDF);
        const res = await fetch('/api/upload/unit/submit', {
          method: "POST",
          body: formData
        })
        if (res.status < 300) {
          _.set(user, ['data', 'units', `${units[unit].code}-${unit}-${version}`, 'status'], "submitted");
          setUser(user);
        }
        return showToast({ type: res.status >= 300 ? 'error' : 'success', text: await res.text() })
      } else {
        const res = await fetch('/api/upload/unit', {
          method: "POST",
          body: JSON.stringify({
            tokenType: 'session',
            unit: `${units[unit].code}-${unit}-${version}`,
            action,
          })
        })
        if (res.status < 300) {
          let newStatus;
          switch (action) {
            case 'interest':
              newStatus = 'interested';
              break
            case 'request':
              newStatus = 'requested';
              break
            case 'drop':
              newStatus = 'dropped';
              break;
          }_.set
          _.set(user, ['data', 'units', `${units[unit].code}-${unit}-${version}`, 'status'], newStatus);
          setUser(user);
        }
        return showToast({ type: res.status >= 300 ? 'error' : 'success', text: await res.text() })
      }
    }

    return <>
      <p>To see all the units and a brief description of each, go to the{" "}
        <NextLink href="resources/catalog">
          <a className="blue-link">unit catalog</a>
        </NextLink>
        .
      </p>
      <p>
        To view past lectures, visit the{" "}
        <NextLink href="home/lectures">
          <a className="blue-link">lecture archives</a>
        </NextLink>
        .
      </p>
      <h2>Unit View</h2>
      {user.data.units && Object.keys(user.data.units).length > 0 ?
        <>
          <div className="rounded-md ring-1 ring-gray-200 dark:ring-gray-700 divide-y divide-solid divide-gray-200 dark:divide-gray-700 my-4">
            <div className="px-4 py-3 dark:bg-gray-900 flex flex-row justify-between rounded-t-md">
              <div className="w-32 flex">Code</div>
              <div className="w-32 flex">Unit Name</div>
              <div className="w-32 flex">Status</div>
            </div>
            {
              Object.keys(user.data.units).map((key, index) => {
                const unit = user.data.units[key]
                if (unit.status === 'dropped') {
                  return null;
                }
                if (filter && (unit.status === 'submitted' || unit.status === 'checked')) {
                  return null;
                }
                return <div
                  className={`px-4 py-3 dark:bg-gray-900 flex flex-row justify-between items-center
            ${index === Object.keys(user.data.units).length - 1 ? 'rounded-b-md' : ''}`}
                  key={key}
                >
                  <div className="w-32 flex">
                    {key.split('-')[0] + '-' + key.split('-')[2]}
                  </div>
                  <div className="w-32 flex flex-col lg:flex-row lg:space-x-1">
                    <div>{['open', 'submitted', 'checked'].indexOf(unit.status) > -1 ? <a href={`/api/files/units/${key.split('-')[0]}-${key.split('-')[1]}?version=${key.split('-')[2]}`} className="blue-link">{units[key.split('-')[1]].name}</a> : units[key.split('-')[1]].name}</div>
                    {['submitted', 'checked'].indexOf(unit.status) > -1 ? <div><a href={`/api/files/units/${key.split('-')[0]}-${key.split('-')[1]}?version=${key.split('-')[2]}&solutions`} className="pink-link">(Solutions)</a></div> : null}
                  </div>
                  <div className="w-32 flex capitalize">
                    {unit.status}
                  </div>
                </div>
              }
              )
            }
          </div>
          <CheckBox text='Hide submitted and checked units' onClick={() => { setFilter(!filter) }} />
        </> : <p>
          You currently have no units. Perhaps you should add a few.
        </p>}

      <h2>Manage Units</h2>
      <div className="space-y-4">
        <ListBox keyVal={true} value={unit} onChange={updateUnit} placeholder='Unit' options={options} />
        <div className="flex flex-row space-x-4">
          <div className="w-1/3">
            <ListBox disabled={!unit} width="w-40" value={version} onChange={updateVersion} placeholder='Version' options={unit ? units[unit].versions : []} />
          </div>
          <div className="w-2/3">
            <ListBox keyVal={true} disabled={!(unit && version)} value={action} onChange={updateAction} placeholder='Action' options={
              ['none', 'dropped'].indexOf(unitStatus()) > -1 && {
                interest: "Mark Interest",
                request: "Request",
              } ||
              "interested" === unitStatus() && {
                request: "Request",
                drop: "Drop"
              } ||
              "requested" === unitStatus() && {
                drop: "Drop"
              } ||
              unitStatus() === 'open' && {
                drop: "Drop",
                submit: "Submit"
              } ||
              ['submitted', 'checked'].indexOf(unitStatus()) > -1 &&
              {
                submit: "Resubmit",
              }
            } />
          </div>
        </div>
        {action === "submit" && <FormFileUpload text="PDF" accept="application/pdf" onChange={(event) => {
          setPDF(event.target.files[0])
        }} />}
        <FormButton text="Perform Action" onClick={() => {
          Action();
        }} />
      </div>
    </>;
  }

  function AdminView() { // Staff is sort of an "admin", so it's still included
    return <>
      <p><i>With great power comes great responsibility.</i></p>
      <p>This is the admin view. You can look at the user view, manage unit requests and applications, and download all the units in the catalogue.</p>
      <p>If you are an admin or superadmin, you can also set the permissions of most users on the User View.</p>
      <div className="grid grid-cols-2 gap-x-8 gap-y-4 pt-2">
        <NextLink href="/home/user-view">
          <a><FormButton text="User View" /></a>
        </NextLink>
        <NextLink href="/resources/catalog">
          <a><FormButton text="Unit Catalogue" /></a>
        </NextLink>
        <NextLink href="/home/lectures">
          <a><FormButton text="Lecture Archive" /></a>
        </NextLink>
        <NextLink href="/home/unit-reqs">
          <a><FormButton text="Unit Requests" /></a>
        </NextLink>
        <NextLink href="/home/manage-apps">
          <a><FormButton text="Applications" /></a>
        </NextLink>
        <NextLink href="/home/unit-subs">
          <a><FormButton text="Unit Submissions" /></a>
        </NextLink>
      </div>
    </>;
  }

  function SuperAdmin() {
    return <>
      <AdminView />
      <div className="pt-2">
        <NextLink href="/home/transfer-superadmin"><a><FormButton text="Transfer Superadmin" color="bg-red-700 hover:bg-red-600 active:bg-red-800" /></a></NextLink>
      </div>
    </>;
  }

  function RenderRole() {
    switch (user.power) {
      case 0:
        return Unverified();
      case 1:
        return User();
      case 2:
        return Applicant();
      case 3:
        return Student();
      case 4:
        return AdminView();
      case 5:
        return AdminView();
      case 6:
        return SuperAdmin();
    }
  }

  return (
    <>
      <h1>Home</h1>
      <p>
        Greetings, <em>{user.first_name}</em>. Your account role is{" "}
        <em>{roleName}</em>.
      </p>
      <RenderRole />
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetch(`${url_prefix}/api/public/units`, { method: "GET" })

  if (res.status >= 300) {
    return {
      props: {
        units: {}
      },
      revalidate: 120
    }
  }

  const units = await res.json()

  return {
    props: {
      units
    },
    revalidate: 120
  }
}
