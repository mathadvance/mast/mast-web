import { useState } from "react";
import { powerToRole } from "@/utils/server/powerToRole";
import { useAuth } from "@/contexts/AuthProvider";
import { FormInput, FormButton } from "@/components/FormComponents";
import { useToast } from "@/contexts/ToastProvider";
import { getUserArray } from "@/utils/server/admin/userArray";

export default function UserList({ userArray }) {

    const { user } = useAuth();
    const [users, setUsers] = useState(userArray || [])
    const [username, setUsername] = useState("");
    const [power, setPower] = useState("");
    const { showToast } = useToast();

    if (!user) {
        return null;
    }

    async function Submit() {
        const res = await fetch("/api/power/change", {
            method: "POST", body:
                JSON.stringify({
                    tokenType: "session",
                    targetUsername: username,
                    power
                })
        })
        if (res.status >= 300) {
            return showToast({ type: "error", text: await res.text() })
        } else {
            showToast({ type: "success", text: `Power of ${username} set to ${power}.` })
            const newUsers = users.map(user => user.username === username ? { ...user, power: parseInt(power) } : user);
            newUsers.sort((a, b) => (a.power > b.power) ? -1 : 1)
            setUsers(newUsers)
            return
        }
    }

    return <>
        <h1>User View</h1>
        {user.power >= 5 && <>
            <h2>Set User Power</h2>
            <p>Each role is associated with a numerical power. Here is the corresponding role for each power:</p>
            <ul>{
                Array.from(Array(7), (e, index) => {
                    return <li className="flex flex-row" key={index}><em className="w-5">{index}</em><div className="capitalize">{powerToRole(index).toLowerCase()}</div></li>
                })
            }
            </ul>
            {user.power === 6 ? <p>You are a superadmin, so you can set anyone's power to an integer from 0 to 5, besides your own.</p> : <>
                <p>You are an admin, so you can set anyone's power to an integer from 0 to 5, besides any superadmins or admins.</p>
                <p>This means that you can promote other people to admins, but you cannot demote them afterwards. Use this power wisely.</p>
            </>}
            <div className="pt-2 space-y-4">
                <FormInput placeholder="Username" onChange={(event) => { setUsername(event.target.value) }} />
                <FormInput placeholder="New Power" value={power} pattern={"[0-6]"} onChange={(event) => {
                    setPower((power) => event.target.validity.valid ? event.target.value : power)
                }} />
                <FormButton text="Set Power" onClick={() => {
                    Submit();
                }} />
            </div>
        </>
        }
        <h2>User List</h2>
        <div className="rounded-md ring-1 ring-gray-200 dark:ring-gray-700 divide-y divide-solid divide-gray-200 dark:divide-gray-700">
            <div className="px-4 py-3 dark:bg-gray-900 flex flex-row justify-between rounded-t-md">
                <div className="w-32 flex items-center">Username</div>
                <div className="w-32 flex items-center">Real Name</div>
                <div className="w-32 flex items-center">User Role</div>
            </div>
            {users.map((user, index) => {
                return <div
                    className={`px-4 py-3 dark:bg-gray-900 flex flex-row justify-between
                ${index === users.length - 1 && `rounded-b-md`}`}
                    key={index}
                >
                    <div className="w-32 flex items-center">{user.username}</div>
                    <div className="w-32 flex items-center">{user.first_name} {user.last_name}</div>
                    <div className="w-32 flex items-center capitalize">{powerToRole(user.power).toLowerCase() + ` (${user.power})`}</div>
                </div>
            })}
        </div>
    </>
}

export async function getServerSideProps(context) {
    const userArray = await getUserArray(context)
    if (!userArray) {
        return {
            notFound: true
        }
    }
    return {
        props: {
            userArray
        }
    }
}