import { useAuth } from "@/contexts/AuthProvider";
import router from "next/router";
import { FormInput, FormButton } from "@/components/FormComponents";
import { useState } from "react";
import { useToast } from "@/contexts/ToastProvider";

export default function Transfer() {
    const { user, setUser } = useAuth();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { showToast } = useToast();
    
    const [transferred, setTransferred] = useState(false);
    if (user && user.power != 6 && !transferred) {
        router.push("/home")
        return null
    }

    if (transferred) {
        return <>
            <h1>Transfer Superadmin</h1>
            <p>Your superadmin role has been transferred to <em>{username}</em>. You are now a regular admin.</p>
        </>
    }

    async function Submit() {
        const res = await fetch("/api/power/superadmin", {
            method: "POST", body:
                JSON.stringify({
                    username: user.username,
                    password,
                    targetUsername: username
                })
        })
        if (res.status >= 300) {
            const errorText = await res.text();
            if (errorText === "Incorrect username/password combination.") {
                showToast({type: "error", text: "Incorrect password."});
                return
            }
            else {
                showToast({type: "error", text: errorText});
                return
            }
        }
        setTransferred(true)
        user.power = 5;
        setUser(user)
        showToast({type:"success",text:`Superadmin transferred to ${username}.`})
    }

    return <>
        <h1>Transfer Superadmin</h1>
        <p>Enter the username of the new superadmin and authenticate to transfer the role.</p>
        <p><em>WARNING:</em> You will be turned into a regular admin. Do not proceed unless you are certain.</p>
        <div className="pt-2 space-y-4">
            <FormInput placeholder="Username" desc="Enter the username of the new superadmin." onChange={(event) => { setUsername(event.target.value) }} />
            <FormInput placeholder="Password" type="password" desc="This should be your own password." onChange={(event) => { setPassword(event.target.value) }} />
            <FormButton text="Transfer Superadmin" onClick={() => {
                if (password.length < 8)
                    return showToast({type: "error", text: "Incorrect password."})
                else
                    Submit();
            }} />
        </div>
    </>
}