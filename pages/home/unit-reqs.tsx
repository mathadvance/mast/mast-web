import { getUsers } from "@/utils/server/admin/users";
import Disclosure from "@/components/Headless/Disclosure";
import { FormButton } from "@/components/FormComponents";

import { useState } from "react";
import { useToast } from "@/contexts/ToastProvider"

export default function UnitRequests(props) {

    const [users, setUsers] = useState(props.users || {})
    const { showToast } = useToast();

    async function Open({ username, unit }) {
        const res = await fetch('/api/staff/unit/req', {
            method: "POST",
            body: JSON.stringify({
                username,
                unit,
                tokenType: 'session'
            })
        })
        if (res.status >= 300) {
            return showToast({ type: 'error', text: 'Unit could not be opened.' });
        }
        showToast({ type: 'success', text: 'Unit successfully opened.' })
        users[username].data.units[unit].status = 'open'
        setUsers({ ...users })
    }

    return <>
        <h1>Unit Requests</h1>
        <p>There is currently no way to reject a request, since it is exceedingly unlikely that a student does abuse the platform.</p>
        <p>If you do see a student potentially requesting too many units, please let them know. If it seems intentional, let me know as well.</p>
        <h2>Requests</h2>
        {Object.keys(users).map((key) => {
            const user = users[key];

            if (user.power != 3 || !user.data.units) {
                return null;
            }

            let completedUnits = 0;
            Object.keys(user.data.units).forEach((key) => {
                if (['submitted', 'resubmitted', 'checked'].indexOf(user.data.units[key].status) > -1) {
                    completedUnits++;
                }
            })

            let openUnits = 0;
            Object.keys(user.data.units).forEach((key) => {
                if (user.data.units[key].status === 'open') {
                    openUnits++;
                }
            })

            const requestedUnits = {};

            Object.keys(user.data.units).forEach((key) => {
                if (user.data.units[key].status === 'requested') {
                    requestedUnits[key] = user.data.units[key]
                }
            })

            if (Object.keys(requestedUnits).length === 0) {
                return null;
            }

            return <Disclosure key={key} text={user.username} backgroundColor="bg-blue-600" borderColor="border-blue-600">
                <div className='space-y-1'>
                    <div><em>Real Name:</em> <span>{user.first_name + ' ' + user.last_name}</span></div>
                    <div><em>Completed Units:</em> <span>{completedUnits}</span></div>
                    <div><em>Open Units:</em> <span>{openUnits}</span></div>
                    {Object.keys(requestedUnits).map((key) =>
                        <div key={`${user}-${key}`} className="pt-1">
                            <FormButton text={`Open ${key}`} color="bg-green-600 hover:bg-green-500 active:bg-green-700" onClick={() => {
                                Open({ username: user.username, unit: key })
                            }} />
                        </div>
                    )}
                </div>
            </Disclosure>
        })}
    </>;
}

export async function getServerSideProps(context) {
    const users = await getUsers(context);
    if (!users) {
        return {
            notFound: true
        }
    }
    return {
        props: {
            users
        }
    }
}