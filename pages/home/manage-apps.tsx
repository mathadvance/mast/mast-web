// For staff and above to grade diagnostics

import { getAppArray } from "@/utils/server/admin/appArray";
import { useState } from "react";
import Disclosure from "@/components/Headless/Disclosure";
import { bgQuestions, contestQuestions } from "@/utils/applications/Season4Questions"
import { FormButton } from "@/components/FormComponents";

import { useToast } from "@/contexts/ToastProvider";

export default function Apps({ appArray }) {

    const [apps, setApps] = useState(appArray)
    const { showToast } = useToast();

    async function SendDecision({ username, decision }) {
        const res = await fetch('/api/staff/app', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                username,
                decision,
                tokenType: 'session',
            })
        })
        if (res.status >= 300) {
            return showToast({ type: 'error', text: await res.text() })
        } else {
            apps[apps.findIndex(app => app.username === username)].decision = decision
            setApps(apps)
            return showToast({ type: 'success', text: await res.text() });
        }
    }

    function Application({ app }) {
        return <div className="space-y-1">
            <div>
                <em>Real Name:</em> <span>{app.first_name + ' ' + app.last_name}</span>
            </div>
            <div>
                <em>Background Questions:</em>
                <ol className="list-decimal list-inside">
                    {bgQuestions.map(question =>
                        <li key={`${question.category}-${question.answer}`} className="font-bold">
                            <span className="font-normal">{question.question}</span>
                            <div className="font-normal italic">{app.answers[question.category][question.answer]}</div>
                        </li>
                    )}
                </ol>
                <em>Contest Questions:</em>
                <ol className="list-decimal list-inside">
                    {contestQuestions.map(question =>
                        <li key={`${question.category}-${question.answer}`} className="font-bold">
                            <span className="font-normal">{question.question}</span>
                            <div className="font-normal italic">{app.answers[question.category][question.answer]}</div>
                        </li>
                    )}
                </ol>
                <div>
                    <em>Diagnostic PDF: </em>
                    <a href={`/api/files/applications/${app.username}`} className='blue-link'>Download</a>
                </div>
                {!app.decision && <div className="w-full gap-x-4 flex flex-row pt-2">
                    <FormButton text="Accept" color="bg-green-600 hover:bg-green-500 active:bg-green-700" onClick={() => {
												if (window.confirm("Really accept?"))
                        SendDecision({ username: app.username, decision: 'accept' })
                    }} />
                    <FormButton text="Reject" color="bg-red-700 hover:bg-red-600 active:bg-red-800" onClick={() => {
												if (window.confirm("Really reject?"))
                        SendDecision({ username: app.username, decision: 'reject' })
                    }} />
                </div>}
            </div>
        </div>
    }

    function UnprocessedApp({ app }) {
        return <Disclosure text={app.username} backgroundColor="bg-blue-600" borderColor="border-blue-600">
            <Application app={app} />
        </Disclosure>
    }

    function ProcessedApp({ app }) {
        return <Disclosure text={app.username} backgroundColor={app.decision === "accept" ? "bg-green-600" : "bg-red-600"} borderColor={app.decision === "accept" ? "border-green-600" : "border-red-600"}>
            <Application app={app} />
        </Disclosure>
    }

    return <>
        <h1>Manage Applications</h1>
        <h2>Unprocessed Applications</h2>
        <p>These applicants have not been accepted or rejected yet.</p>
        {apps.filter(app => !app.decision).map(app =>
            <UnprocessedApp app={app} key={app.username} />
        )}
        <h2>Processed Applications</h2>
        <p>A decision has already been made for these applicants.</p>
        {apps.filter(app => app.decision).map(app =>
            <ProcessedApp app={app} key={app.username} />
        )}
    </>;
}

export async function getServerSideProps(context) {
    const appArray = await getAppArray(context)
    if (!appArray) {
        return {
            notFound: true
        }
    }
    return {
        props: {
            appArray
        }
    }
}
