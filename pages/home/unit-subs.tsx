import { getUsers } from "@/utils/server/admin/users";
import Disclosure from "@/components/Headless/Disclosure";
import { FormTextArea, FormButton } from "@/components/FormComponents";
import { useToast } from "@/contexts/ToastProvider";

import { useState } from "react";

export default function UnitSubs(props) {
    const { showToast } = useToast();
    const [users, setUsers] = useState(props.users || {})
    const [comments, setComments] = useState({});

    async function Check({ username, unit }) {
        const res = await fetch('/api/staff/unit/sub', {
            method: "POST",
            body: JSON.stringify({
                username,
                unit,
                comments: comments[username],
                tokenType: 'session'
            })
        })
        if (res.status >= 300) {
            showToast({ type: 'failure', text: 'Unit could not be returned.' })
        }
        showToast({ type: 'success', text: 'Unit successfully returned.' })
        users[username].data.units[unit].status = 'checked'
        setUsers({ ...users })
    }

    return <>
        <h1>Unit Submissions</h1>
        {Object.keys(users).map((key) => {
            const user = users[key];

            if (user.power != 3 || !user.data.units) {
                return null;
            }

            let submittedUnits = 0;
            Object.keys(users[key].data.units).forEach((key) => {
                if (user.data.units[key].status === 'submitted' || user.data.units[key].status === 'resubmitted') {
                    submittedUnits++;
                }
            })
            if (submittedUnits === 0) {
                return null;
            }
            return <Disclosure text={key} backgroundColor="bg-blue-600" borderColor="border-blue-600">
                <div className='space-y-1'>
                    <h2>Information</h2>
                    <div><em>Real Name:</em> <span>{user.first_name} {user.last_name}</span></div>
                    {Object.keys(user.data.units).map((key) => {
                        const unit = user.data.units[key];
                        if (unit.status === 'submitted' || unit.status === 'resubmitted')
                            return <>
                                <h2>{key} (<span className="capitalize">{unit.status}</span>)</h2>
                                <div><em>Unit Submission:</em> <span><a href={`/api/files/unit-submissions/${user.username}/${key}`} className="blue-link">Download</a></span></div>
                                <div className="space-y-3 pt-1">
                                    <FormTextArea placeholder="Comments..." onChange={(event) => {
                                        comments[user.username] = event.target.value
                                        setComments(comments)
                                    }} />
                                    <FormButton text="Return With Comments" onClick={() => {
                                        Check({ username: user.username, unit: key })
                                    }} />
                                </div>
                            </>
                    })}
                </div>
            </Disclosure>
        })}
    </>;
}

export async function getServerSideProps(context) {
    const users = await getUsers(context);
    if (!users) {
        return {
            notFound: true
        }
    }
    return {
        props: {
            users
        }
    }
}