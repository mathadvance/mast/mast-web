import { createContext, useContext, useState } from "react";
const ToastContext = createContext(null);

export default function ToastProvider({children}) {
    const [toast, setToast] = useState({});
    const [timer, setTimer] = useState(undefined);

    function showToast(toast) {
        clearTimeout(timer)
        setToast(toast)
        setShow(true)
        const newTimer = setTimeout(() => {
            setShow(false)
        }, 3000)
        setTimer(newTimer)
    }
    const [show, setShow] = useState(false);
    // type: error, success
    // text

    return (
        <ToastContext.Provider value={{toast, showToast, show, setShow}}>
            {children}
        </ToastContext.Provider>
    )
}

export function useToast() {
    return useContext(ToastContext)
}