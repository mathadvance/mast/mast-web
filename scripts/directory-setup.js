const fs = require("fs");
const shell = require("shelljs");
const path = require("path");
require("dotenv").config({
  path: path.resolve(".env.local"),
});

async function makeDir(pathArray) {

  const dirPath = pathArray ? path.join(process.env.RESOURCE_DIR, pathArray) : process.env.RESOURCE_DIR

  await fs.mkdir(
    dirPath,
    { recursive: true },
    (err) => {
      if (err) {
        return console.error(err);
      }
    }
  );
}

async function setup() {

  // Await make parent dir (wouldn't be necessary if not for shelljs)
  // We don't slap an await onto some upload dir function because that looks stupid

  await makeDir()

  // Make all dirs in upload dir

  makeDir("uploads", "applications");
  makeDir("uploads", "units");
  makeDir("uploads", "staff_pictures");

  // Make content dir and clone 

  const res = shell.cd(process.env.RESOURCE_DIR)
  if (res.stderr) {
    console.log("Error: Somehow, the makeDir function failed.")
    return
  }
  shell.exec(`git clone ${process.env.CONTENT_REPO} content`)
  shell.cd("content") // Relative cd
  shell.exec("git submodule update --init")
}

setup();